const Server = window.env.SERVER_IPADDRESS;
const Port = window.env.PORT;
const ProxySocket = window.env.PROXY_IPADDRESS;
const ProxyPort = window.env.PROXY_PORT;

const Path = 'metricanalyzer'
const Apis = ApiAddress(Server, Port, Path , ProxySocket , ProxyPort)
function ApiAddress(Server, Port, Path , ProxyServer , ProxyPort) {
    return {
        registerdata: `${Server}:${Port}/${Path}/registerdata`,
        gettrainedmodels: `${Server}:${Port}/${Path}/gettrainedmodels`,
        startdetectanomalies: `${Server}:${Port}/${Path}/startdetectanomalies`,
        stopdetectanomalies: `${Server}:${Port}/${Path}/stopdetectanomalies`,
        getmetricvalues: `${Server}:${Port}/${Path}/getmetricvalues`,
        getmetrics: `${Server}:${Port}/${Path}/getmetrics`,
        proxysocket :`${ProxyServer}:${ProxyPort}/proxysockethub`
    }
}
export default Apis
