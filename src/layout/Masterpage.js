import React  from 'react'
import { useSelector  } from 'react-redux'
// Component
import Header from './Header'
import Sidebar from './Sidebar'
import Footer from './Footer'
// Css
import "./Masterpage.css"


const Masterpage = ({ children }) => {
    const { masterpage } = useSelector(state => state);
    const { data, sideBarToggle } = masterpage;
    if (!data || data === null) return null;

    return (
        <div className={sideBarToggle ? "wrapper" : "wrapper active"}>
            <Header />
            <div className="main_body">
                <Sidebar />
                <div className="container">
                    {children}
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Masterpage;
