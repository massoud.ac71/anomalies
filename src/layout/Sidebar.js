import React, { memo } from 'react';
import { NavLink } from "react-router-dom"

// import masterpageContext from './Context'
import { toggleSidebar, handleSidebarMenu } from './../state/layout/masterpageActions';
import { useDispatch, useSelector } from 'react-redux';
const Sidebar = () => {
    const dispatch = useDispatch()
    const { masterpage } = useSelector(state => state)
    const { sideBarToggle, data } = masterpage
    const renderLink = (node) => {
        return (
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                    <span className="icon">
                        <i className={node.icon}></i>
                    </span>
                    <span className="list">{node.title}</span>
                </div>
                {node.subMenu && sideBarToggle ? (
                    <div>
                        <span><i className={node.status ? "fa fa-chevron-down" : "fa fa-chevron-left"}></i></span>
                    </div>
                ) : null}

            </div>
        )
    }
    const navLinks = (node) => {
        return (
            <NavLink to={node.link}>
                {renderLink(node)}
            </NavLink>
        )
    }
    const disbaleLink = (node) => {
        return (
            <div onClick={(e) => dispatch(handleSidebarMenu(node, e))}>
                <a>
                    {renderLink(node)}
                </a>
            </div>
        )
    }
    const createList = () => {
        return (
            <ul className="siderbar_main_menu">
                {data.map((node, index) => {
                    return (
                        <li key={node.id}>
                            {(node?.link && node.link !== null) && navLinks(node)}
                            {(!node?.link || node?.link === null) && disbaleLink(node)}
                            {node.subMenu && sideBarToggle ?
                                <div className={node.status ? "inner__sidebar_submenu active" : "inner__sidebar_submenu"}>
                                    <ul >
                                        {node.subMenu.map((o, i) => {
                                            return (
                                                <li key={i}>
                                                    <NavLink to={o.link}>
                                                        <span className="icon">
                                                            <i className={o.icon}></i>
                                                        </span>
                                                        <span className="list">{o.title}</span>
                                                    </NavLink>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                : null}
                        </li>
                    );
                })}
            </ul>
        )
    }
    const hanldeHamburger = () => {
        return (
            <div className="hamburger" onClick={() => dispatch(toggleSidebar())} >
                <div className="inner_hamburger" >
                    <span className="arrow">
                        <i className="fa fa-long-arrow-left" aria-hidden="true">
                        </i>
                        <i className="fa fa-long-arrow-right" aria-hidden="true">
                        </i>
                    </span>
                </div>
            </div>
        )
    }
    return (
        <div className="sidebar_menu">
            <div className="inner__sidebar_menu">
                <div className="sidebar_menu__list">
                    <div className="title">
                        <span>M</span>
                        <span>etric Analyzer</span>
                    </div>
                    {createList()}
                </div>
                {hanldeHamburger()}
            </div>
        </div>
    );
}
export default memo(Sidebar);
