import React, { PureComponent } from 'react'

class Footer extends PureComponent {
    render() {
        return (
            <div className="bottom_footer">
                <div className="bottom_menu">
                    <div className="home_link">
                        <div>
                            <span className="icon">
                                <i className="fa fa-copyright" aria-hidden="true"></i>
                            </span>
                            <span> تمامی حقوق نرم افزار متعلق به شرکت صنایع الکترونیک زعیم می باشد.</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;