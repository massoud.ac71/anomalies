import { combineReducers } from 'redux'
import { masterpageReducer } from './layout/masterpageReducer';

export const reducers = combineReducers({
    masterpage: masterpageReducer,
})