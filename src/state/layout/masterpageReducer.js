import { basicSidebar } from '../../data/BasicMenu.json'

const initialState = {
    sideBarToggle: true,
    data: basicSidebar,
}
export const masterpageReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SIDEBAR_TOGGLE":
            return {
                ...state,
                sideBarToggle: !state?.sideBarToggle
            }
        case "SIDEBAR_MENU":
            return {
                ...state,
                data : action.payload,
                sideBarToggle: true
            }
        case "CLEAR_STATE":
            return initialState
        default:
            return state;
    }
}
