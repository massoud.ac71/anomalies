export const toggleSidebar = () => {
    return async (dispatch, getState) => {
        const { masterpage } = getState();
        const { data, sideBarToggle } = masterpage
        const res = [...data];
        if (!sideBarToggle)
            res.forEach((o) => o.status = false)
        await dispatch({ type: "SIDEBAR_TOGGLE", payload: !sideBarToggle });
    };
};
export const handleSidebarMenu = (node, e) => {
    return async (dispatch, getState) => {
        const { masterpage } = getState()
        const { data } = masterpage
        e.preventDefault()
        const result = Array.from(data)
        if (!node.status) {
            result.forEach((o) => {
                if (o.id === node.id) {
                    o.status = true
                }
                else {
                    o.status = false
                }
            })
        }

        else {
            if (node.status === true)
                result.forEach((o) => {
                    if (o.status === node.status) {
                        o.status = false
                    }
                })
        }
console.log("xx")
        await dispatch({ type: "SIDEBAR_MENU", payload: result });
    }
}
export const clearState = () => {
    return async dispatch =>
        await dispatch({ type: "CLEAR_STATE", payload: "" })
}
