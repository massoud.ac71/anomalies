import React from 'react'
import { ToastContainer } from 'react-toastify';


const ToastContainers = ({ 
    position,
    containerId
}) => {
    return (
        <ToastContainer
            position={position}
            containerId={containerId}
            autoClose={2500}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={true}
            limit={1}
            pauseOnFocusLoss
            draggable={false}
            enableMultiContainer
            pauseOnHover
        />
    );
}

export default ToastContainers;