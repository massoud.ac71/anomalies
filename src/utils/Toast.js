
import Swal from 'sweetalert2'
import { toast } from 'react-toastify';

var Notify = Swal.mixin({
    toast: true,
    position: "bottom-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener("mouseleave", Swal.resumeTimer)
    }
})
class SnackBar {
    Success = (msg) => {

        Notify.fire({
            icon: "success",
            title: msg ? msg : "عملیات با موفقیت انجام شده است"
        })
    }
    Warinig = (msg) => {
        Notify.fire({
            icon: "warning",
            title: msg ? msg : "عملیات با مشکل مواجه شده است"
        })
    }
    Error = (msg) => {
        Notify.fire({
            icon: "error",
            title: msg ? msg : "عملیات با خطا مواجه شده است"
        })
    }
    // ReactToastify
    ToastWarining = (text, containerId) => {
        toast.warning(text, {
            autoClose: 2000,
            closeOnClick: true,
            containerId,
        });
    }
    ToastDefault = (text, containerId) => {
        toast(text, {
            autoClose: 2000,
            closeOnClick: true,
            containerId,
        });
    }
    ToastError = (text, containerId) => {
        toast.error(text ? text : "خطا رخ داده است ", {
            autoClose: 2000,
            closeOnClick: true,
            containerId,
        })
    }
    ToastSuccess = (text, containerId) => {
        toast.success(text ? text : "عملیات با موفقیت انجام شد", {
            autoClose: 2000,
            closeOnClick: true,
            containerId,
        })
    }
    ToastNotify = (text, containerId) => {
        toast.info(text, {
            autoClose: 1500,
            closeOnClick: true,
            containerId,
        })
    }
}
const _SnackBar = new SnackBar();
export default _SnackBar
