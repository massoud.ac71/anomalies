import React from 'react';
import { Input, Select, DatePicker, DateTimePicker, CheckBox } from '../component/Form'
import PropTypes from 'prop-types';

const DynamicForm = ({ item, changeHandler }) => {
    return (
        item.map((o, i) => {
            let req = typeof o.required === "boolean" ? o.required : JSON.parse(o.required);
            if (o.type === "text" || o.type === "mask") {
                return (
                    <div className="model-item" key={i}>
                        <Input
                            type={o.type}
                            required={req}
                            label={o.label}
                            value={o.value}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            } else if (o.type === "select" || o.type === "Unit-select") {
                return (
                    <div className="model-item" key={i}>
                        <Select
                            type={o.type}
                            required={req}
                            items={o.items}
                            mapping={o.mapping}
                            label={o.label}
                            value={o.value}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            } else if (o.type === "number-input" || o.type === "number") {
                return (
                    <div className="model-item" key={i}>
                        <Input
                            type={o.type}
                            required={req}
                            label={o.label}
                            value={o.value}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            } else if (o.type === "datepicker") {
                return (
                    <div className="model-item">
                        <DatePicker
                            key={i}
                            required={req}
                            label={o.label}
                            value={o.value}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            }
            else if (o.type === "date-time-picker") {
                return (
                    <div className="model-item" key={i}>
                        <DateTimePicker
                            key={i}
                            required={req}
                            label={o.label}
                            value={o.value}
                            timer={o.timer}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            } else if (o.type === "textarea") {
                return (
                    <div className="model-item" key={i}>
                        <Input
                            type={o.type}
                            required={req}
                            label={o.label}
                            value={o.value}
                            multiline={true}
                            change={changeHandler.bind(this, o.field, item)}
                        />
                    </div>
                );
            } else if (o.type === "checkbox") {
                return (
                    <div className="modal-item">
                        <CheckBox
                            key={i}
                            required={req}
                            label={o.label}
                            value={o.value}
                            change={changeHandler.bind(this, o.field)}
                        />
                    </div>
                )
            }
        })
    );
}
DynamicForm.propTypes = {
    items: PropTypes.array,
    changeHandler: PropTypes.func
}
export default DynamicForm;
