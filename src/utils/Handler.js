import Toast from './Toast'

/**
 * Handle api errors 
 */
class Handler {
    apiHandler(response = {}, callback) {
        if (response.data && response.data.Error) {
            this.message(response.data.Message)
        }
        callback(response.data, response.status)
    }
    message(msg) {
        Toast.ToastError(msg,"A")
    }
}


const _Handler = new Handler();
export default _Handler