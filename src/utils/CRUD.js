import staticData from '../data/Static'
import Handler from './Handler';
import moment from 'moment-jalaali';
import {CustomErrorHandler} from "../services/CustomErrorHander";
import {postMethod} from './../services/RestApi';

class CRUD {
    ConverterJson = (data) => {
        let obj = {};
        let IsUniq = {}
        data.items.forEach((n, i) => {
            if (n.type === "select") {
                obj[n.field] = n.value ? (n.value.id ? n.value.id : n.value) : null;
            } else if (n.type === "datepicker") {
                obj[n.field] = n.value
                    ? n.value.dateStr === null
                        ? ""
                        : n.value.dateStr
                    : "";
            } else if (n.type === "date-time-picker") {
                let x = new Date(n.value),
                    month = '' + (x.getMonth() + 1),
                    day = '' + x.getDate(),
                    year = x.getFullYear(),
                    hour = '' + x.getHours(),
                    minutes = '' + x.getMinutes(),
                    second = '' + x.getSeconds();
                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;
                if (hour.length < 2)
                    hour = '0' + hour
                if (second.length < 2)
                    second = '0' + second
                if (minutes.length < 2)
                    minutes = '0' + minutes;
                const date = [year, month, day].join('-');
                const time = [hour, minutes, second].join(':');
                const dates = date + 'T' + time;
                debugger
                obj[n.field] = dates;
            } else if (n.type === "selectcolor") {
                obj[n.field] = n.value.id
            } else if (n.type === "checkbox") {
                obj[n.field] = n.value !== "" ? n.value : false;
            } else if (n.type === "number") {
                obj[n.field] = n.value !== "" ? Number(n.value) : 0;
            } else if (n.type === "segemnt-picker") {
                obj["Colors"] = n.value.Color ? [...n.value.Color] : null
                obj["Color"] = n.value.Color ? n.value.Color[0] : null
                obj["Segments"] = n.value.Segment ? [...n.value.Segment] : null
                obj["Min"] = n.value.Segment ? n.value.Segment[0] : null
                obj["Max"] = n.value.Segment ? n.value.Segment[n.value.Segment.length - 1] : null
            } else if (n.type === "metrics") {
                obj[n.field] = {
                    ProjectId: 1,
                    Name: ""
                }
            } else if (n.type === "Unit-select") {
                obj[n.field] = n.value.name
            } else if (n.type === "textarea") {
                obj[n.field] = n.value;
            } else {
                obj[n.field] = n.value;
            }

            if (n.IsUnique === "true") {
                if (n.type === "select") {
                    IsUniq[n.field] = n.value ? (n.value.id ? n.value.id : n.value) : null;
                } else if (n.type === "datepicker") {
                    IsUniq[n.field] = n.value
                        ? n.value.dateStr === null
                            ? ""
                            : n.value.dateStr
                        : "";
                } else if (n.type === "checkbox") {
                    IsUniq[n.field] = n.value !== "" ? n.value : false;
                } else if (n.type === "number") {
                    IsUniq[n.field] = n.value !== "" ? n.value : 0;
                } else {
                    IsUniq[n.field] = n.value;
                }
            }
        });
        return obj
    }
    FormValidate = (data) => {
        let valid = true;
        for (let i = 0; i < data.length; i++) {
            if (data[i].required === true) {
                if (data[i].value === "") {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    }
    ConvertDateToMoment = (data) => {
        let d = new Date(data),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear(),
            hour = '' + d.getHours(),
            minutes = '' + d.getMinutes(),
            second = ''+ d.getSeconds();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        if(hour.length < 2)
            hour ='0'+hour
        if(second.length <2)
            second = '0'+second
        if(minutes.length<2)
            minutes = '0'+minutes;
        const date = [year, month, day].join('-');
        const time = [hour,minutes,second].join(':');
        const dates = date+' '+time;
        return  moment(dates,'YYYY-M-D HH:mm:ss').format('jYYYY/jM/jD HH:mm:ss');
    }
    loadGrid = async (pageSize, pageNumber, columns, api, request) => {
        // const requestfilter = columns.filter(node => node.filterText.trim() !== "").map((o, index) => {
        //     return {
        //         filed: o.title,
        //         type: o.filterType,
        //         searchText: o.filterText
        //     }
        // })
        // const requestSort = columns.filter(node => node.sort.show).map((o, index) => {
        //     if (o.sort.show) {
        //         return {
        //             field: o.title,
        //             type: o.sort.desc ? "desc" : "asc"
        //         }
        //     }
        // })
        // const request = {
        //     pageSize,
        //     pageNumber,
        //     filter: requestfilter,
        //     sort: requestSort
        // }
        // let data;
        // try {
        //     Handler.apiHandler((await getGridData(config.GetWarrent, request)), (response, status) => {
        //         if (status === 200) {
        //             data = response.value
        //         }
        //     })
        // } catch (error) {
        //     Toast.Error("شماره ای با مشخصات ذکر شده یافت نشد")
        // }
        // return data
        let data = null
        let d;
        try {
            Handler.apiHandler(await postMethod(api, request), (response, status) => {
                if (response?.value?.statusCode === 200 && status === 200) {
                    data = response?.value?.dtos
                    d = new Promise(resolve => {
                        let serverData = data
                        setTimeout(() => {
                            var filterCount = columns.filter(o => o.filterText.trim() !== '');
                            if (columns.length !== 0 && filterCount.length !== 0) {
                                let temp = [];
                                columns.forEach((o, i) => {
                                    serverData.forEach((x, i) => {
                                        // if (String(x[o.title]).toLocaleLowerCase().trim() === String(o.filterText).toLocaleLowerCase().trim()) {
                                        //     temp.push(x)
                                        // }
                                        if (o.filterText !== "")
                                            if (String(x[o.title]).toLocaleLowerCase().trim()
                                                .includes(String(o.filterText).toLocaleLowerCase().trim())) {
                                                temp.push(x)
                                            }
                                    })
                                });
                                serverData = temp
                            }
                            let pagedData = serverData.filter((o, i) => {
                                if (i >= (0 + (pageSize * (pageNumber - 1))) && i < (pageSize * pageNumber)) {
                                    return o
                                }
                            })
                            resolve({
                                Items: pagedData,
                                pageCount: serverData.length
                            })
                        }, 500)
                    });
                } else {
                    CustomErrorHandler(response);
                    d = null;
                }
            })
        } catch (error) {
        }
        return d;
    }
}

const _CRUD = new CRUD()
export default _CRUD
