import $ from 'jquery'

$.each($("textarea[data-autoresize]"), function () {
    let offset = this.offsetHeight - this.clientHeight;
    let resizeTextarea = function (el) {
        $(el)
            .css("height", "auto")
            .css("height", el.scrollHeight + offset);
    };
    $(this)
        .on("keyup input", function () {
            resizeTextarea(this);
        })
        .removeAttr("data-autoresize");
});
$(document).ready(function () {
    $("input[type=number-input]").keypress(function (e) {
        let key = e.keyCode;
        if (key === "0".charCodeAt(0) && $(this).val().trim() === "") {
          return false
        }
        if (key !== 8 && key !== 0 && (key < 48 || key > 57)) {
            e.preventDefault();
        }
    });
});
  // $("input[type='mask']").on("keyup", function () {
    //   $("input[type='mask']").val(destroyMask(this.value));
    //   this.value = createMask($("input[type='mask']").val());
    // })
    // function createMask(string) {
    //   return string.replace(/(\d{3})(\d{3})(\d{3})(\d{3})/, "$1.$2.$3.$4");
    // }
    // function destroyMask(string) {
    //   return string.replace(/\D/g, '').substring(0, 12);
    // }
    //inputOnlyString
    // (key >= 48 && key <= 57)
