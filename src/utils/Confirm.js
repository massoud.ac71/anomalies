import Swal from 'sweetalert2'

class Confirm {
    ConfirmForm = async (index) => {
        let result;
        await Swal.fire({
            title: index,
            icon: "error",
            direction: "rtl",
            showCancelButton: true,
            cancelButtonText: "خیر",
            confirmButtonText: "بله",
            buttonsStyling: false,
            width: "300px",
            padding: "0",
            customClass: {
                confirmButton: "swal confirm_button",
                cancelButton: "swal cancel_button",
                title: "swal title",
                icon: "swal error_icon",
                actions: "swal action",
            },
            allowOutsideClick: false,
            allowEscapeKey: false,
        }).then((resolve) => {
            if (resolve.value) {
                result = true;
            } else {
                result = false
            }
            ;
        })
        return result
    }
}

const _Confirm = new Confirm()
export default _Confirm
