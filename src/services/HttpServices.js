import axios from 'axios'
import Cookie from 'js-cookie';

axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["Authorization"] = "Bearer  " + Cookie.get('token')


axios.interceptors.response.use(response => response, (error) => {
    try {
        if (!error.response || error.response.status === 404 || error.response.status === 504 || error.response.status === 500 || error.response.status === 501) {
            return Promise.resolve(
                {
                    data: {
                        Message: "عدم اتصال به پایگاه داده",
                        Error: true,
                    }
                }
            )
        }
        else {
            if (error.response.status === 401 || error.response.status === 403) {
                return Promise.resolve(
                    {
                        data: {
                            Message: "عدم اعتبار سنجی درست",
                            Error: true
                        }
                    }
                )
            }
            else {
                return Promise.resolve(
                    {
                        data: {
                            Message: error,
                            Error: true
                        }
                    }
                )
            }
        }
    }
    catch (error) {

    }
})
export default {
    get: axios.get,
    post: axios.post,
    put: axios.put,
    delete: axios.delete
}