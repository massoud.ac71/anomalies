import Toast from "../utils/Toast";

export const CustomErrorHandler = (response)=>{
    const {value} = response
    const {errorMessage , statusCode} = value
    Toast.ToastError(`${statusCode} : ${errorMessage}`,"A");
}
