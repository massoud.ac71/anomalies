import httpService from './HttpServices'

export function postMethod(url, data) {
    return httpService.post(url, data)
}
export function getMethod(url){
    return httpService.get(url);
}