import React, { Fragment } from 'react';
import Routes from './Routes';
import { StoreProvider } from './globalStore/GlobalStore';
import {BrowserRouter} from 'react-router-dom'
import ToastContainers from './utils/ToastContainer';
import Cookie from 'js-cookie';
// import './component/Modal/Modal.css'

const initialValue = {
  name: "Bina-System"
}
let token = Cookie.get("token")
let isValidUser;
if (token) {
  isValidUser = true
}
else {
  isValidUser = false
}

const App = () => {
  return (
    <StoreProvider store={initialValue}>
      <Fragment>
        <ToastContainers
          containerId={"A"}
          position={"top-right"}
        />
        <BrowserRouter>
          <Routes isAuth={isValidUser} />
        </BrowserRouter>
      </Fragment>
    </StoreProvider>
  );
}

export default App;
