import React, { Component, memo } from 'react'
import CollapseForms from "../../component/CollapseMenu/collapseMenu";
import Apis from '../../data/Api';
import Masterpage from "../../layout/Masterpage";
import { getMethod, postMethod } from '../../services/RestApi';
import Handler from './../../utils/Handler';
import { CustomErrorHandler } from './../../services/CustomErrorHander';
import { HubConnectionBuilder } from '@microsoft/signalr';
import SnackBar from "../../utils/Toast";
import CRUD from "../../utils/CRUD";
import { StockEventsChart } from "../../component/ReactChart";
import { SpinnerHeartLine } from '../../component/SpinLoader/spinner';
import './trainedModels.scss'
import { stockData, stockEvent , newData } from '../../fake-data/stockEvent';
// method name for active proxySocket connection on
const METHOD_NAME = "DetectedAnomalies";

    class AnomaliesDetect extends Component {
        constructor(props) {
            super(props);
            this.state = {
                //items-form
                items: [
                    {
                        type: "Unit-select", field: "MetricName", label: "نام متریک", value: "", required: true,
                        items: [], mapping: { text: 'name', value: 'id' }
                    },
                    {
                        type: "number",
                        field: "IntervalSeconds",
                        label: "بازه زمانی تشخیص (ثانیه)",
                        value: "5",
                        required: true,
                        timer: true
                    },
                    { type: "number", field: "Mean", label: "میانگین", value: "0", required: true },
                    { type: "number", field: "Std", label: "انحراف معیار", value: "0", required: true },
                    { type: "number", field: "WindowSize", label: "اندازه پنجره", value: "50", required: true },
                    { type: "number", field: "Threshold", label: "حد آستانه تشخیص", value: "10", required: true },
                    { type: "number", field: "PeriodicityMinutes", label: "دوره تناوب(دقیقه)", value: "120", required: true },
                ],
                //scenario-form
                scenarioItems: [
                    {
                        type: "Unit-select", field: "Type", label: "نوع", value: "", required: true,
                        items: [{ id: 1, name: "افزایشی" }, { id: 2, name: "کاهشی" }], mapping: { text: 'name', value: 'id' }
                    },
                    { type: "number", field: "Value", label: "مقدار", value: "100", required: true },
                    { type: "number", field: "RangeSeconds", label: "طول ناهنجاری", value: "5", required: true },
                    { type: "number", field: "IntervalSeconds", label: "فاصله زمانی (ثانیه)", value: "300", required: true },
                ],
                // signalR connection id
                signalRConnectionId: null,
                // preloader
                loader: false,
                // data for chart
                anomaliesData: newData,
                // event data for chart 
                anomaliesEvent: stockEvent,

            }
        }
        //lifecycle method  get metric list and setup signalRconnection
        componentDidMount = () => {
            // از کامنت خارج شود
            // this.getMetrics();
            // this.setupSignalRConnection();
            //********************* */
            // پاک شود
            this.timer = setInterval(() => {
                const anomaliesData = [...this.state.anomaliesData];
                anomaliesData.push(stockData[0])
                if (stockData.length !== 0) {
                    stockData.shift();
                    console.log(`stockData : ${stockData.length}`);
                    console.log(`DataSource : ${anomaliesData.length}`);
                    anomaliesData.sort((a,b)=> a.x - b.x)
                    this.setState({ anomaliesData })
                }
            }, 100)
            //********************** */
        }
        // ********** پاک شود
        componentWillUnmount() {
            clearInterval(this.timer);
        }
        //******************* */
        //set value for items-form
        changeHandlerForms = (field, item, e) => {
            this.changeForm(field, item, e, "detect")
        }
        //set value for scenario-form
        changeHandlerScenario = (field, item, e) => {
            this.changeForm(field, item, e, "scenario")
        }
        // set value input form  for scenario-form and items-form 
        changeForm = (field, item, value, name) => {
            switch (name) {
                case "detect":
                    this.setState(prev => ({
                        ...prev,
                        items: prev.items.map(el => {
                            if (el.field === field) return { ...el, value }
                            else return el
                        })
                    }));
                    break;
                case "scenario":
                    this.setState(prev => ({
                        ...prev,
                        scenarioItems: prev.scenarioItems.map(el => {
                            if (el.field === field) return { ...el, value }
                            else return el
                        })
                    }));
                    break;
                default:
                    break;
            }
        }
        //setup signalR id connection
        setupSignalRConnection = async () => {
            const connection = new HubConnectionBuilder()
                .withUrl(Apis.proxysocket)
                .withAutomaticReconnect()
                .build();
            connection.on(METHOD_NAME, response => {
                // get resposne form proxy socket.
                let result = [...response];
                result.forEach(el => el.DateTime.replace("T", " "));
                const anomaliesData = [...this.state.anomaliesData];
                const anomaliesEvent = [...this.state.anomaliesEvent];
                for (let i = 0; i < result.length; i++) {
                    //updated duplicate date 
                    const findIndex = anomaliesData.indexOf(el=> el.x === result[i].DateTime);
                    if(findIndex){
                        anomaliesData[findIndex].high = result[i].Value;
                    }else {
                        anomaliesData.push({ x: new Date(result[i].DateTime), high: result[i].Value })
                    }
                    if (result[i].IsAnomaly === true) {
                        anomaliesEvent.push({
                            date: new Date(result[i].DateTime),
                            text: 'Anomalies',
                            description: ``,
                            textStyle: { color: '#FFFFFF' },
                            background: '#ED4337',
                            border: { color: '#ED4337' }
                        })
                    }
                    //push new value
                    
                }
                anomaliesData.sort((a,b)=> a.x - b.x)
                this.setState({ anomaliesData, anomaliesEvent });
            })
            try {
                await connection.start()
                    .then(() => {
                        // start signalR connection and set signalR id.
                        this.setState({ signalRConnectionId: connection.connectionId })
                    });
            } catch (error) {
                SnackBar.ToastError("Proxy Socket not Working", "A");
            }

        }
        // get metric list from backend
        getMetrics = async () => {
            Handler.apiHandler(await getMethod(Apis.getmetrics), (response, status) => {
                if (response?.value?.statusCode === 200 && status === 200) {
                    const result = response.value.dtos
                    const items = [...this.state.items];
                    const res = items.map((els, index) => {
                        if (els.field === "MetricName") {
                            for (let i = 0; i < result.length; i++) {
                                els.items.push({ id: i + 1, name: result[i] });
                            }
                            return els;
                        } else return els;
                    })
                    this.setState({ items: res })
                } else if (status === 200 && response?.value?.statusCode !== 200) {
                    CustomErrorHandler(response);
                }
            })
        }
        // start proxy socket
        handleRegister = async () => {
            this.setState({ loader: true })
            const { items, scenarioItems, signalRConnectionId } = this.state;
            const itemsValid = CRUD.FormValidate(items)
            const scenarioValid = CRUD.FormValidate(scenarioItems);
            if (itemsValid && scenarioValid) {
                this.setState({ loader: true })
                const anomaliesJsonItems = CRUD.ConverterJson({ items });
                const scenarioJsonItem = CRUD.ConverterJson({ items: scenarioItems });
                scenarioJsonItem[`Type`] = scenarioItems.Type === "افزایشی" ? 0 : 1
                const request = {
                    ...anomaliesJsonItems,
                    ShiftScenario: { ...scenarioJsonItem },
                    signalRConnectionId,
                    receiverMethodName: METHOD_NAME
                }
                Handler.apiHandler(await postMethod(Apis.startdetectanomalies, request), (response, status) => {
                    if (response?.value?.statusCode === 200 && status === 200) {
                        this.setState({ loader: false })
                        SnackBar.ToastSuccess("پروکسی با موفقیت فعال شد", "A")
                    } else if (response?.value?.statusCode !== 200 && status === 200) {
                        CustomErrorHandler(response);
                        this.setState({ loader: false })
                    }
                })
            } else {
                SnackBar.ToastError("لطفا مقادیر اجباری را وارد کنید", "A");
            }

        }
        // stop proxy socket 
        handleStop = async () => {
            const { items } = this.state
            const res = CRUD.ConverterJson({ items });
            const { MetricName } = res;
            const request = { MetricName }
            try {
                Handler.apiHandler(await postMethod(Apis.stopdetectanomalies, request), (response, status) => {
                    if (response?.value?.statusCode === 200 && status === 200) {
                        SnackBar.ToastSuccess("پروکسی سوکت با موفقیت متوقف گردید", "A")
                    } else if (response?.value?.statusCode !== 200 && status === 200) {
                        CustomErrorHandler(response);
                        this.setState({ loader: false })
                    }
                })
            } catch (ex) {
            }
        }
        handleRefresh = () => {
            this.setState({ refresh: false })
        }
        //jsx main-render page
        render() {
            const { items, scenarioItems, anomaliesData, loader, anomaliesEvent, signalRConnectionId } = this.state
            return (
                <Masterpage>
                    <div className="register-data">
                        <CollapseForms items={items} scenarioItems={scenarioItems} Change={this.changeHandlerForms}
                            register={this.handleRegister} txtBtn={"شروع"}
                            changeSenario={this.changeHandlerScenario}
                            divider={true} tag={"شبیه ساز ناهنجاری"} />
                        <div className="register-chart">
                            {loader === true && <SpinnerHeartLine />}
                            {loader === false &&
                                <>
                                    {signalRConnectionId !== null && <div className="stop-anomalies" onClick={this.handleStop}>
                                        <span><i className="mdi mdi-stop-circle" /></span>
                                    </div>}
                                    <StockEventsChart
                                        id={'trained-models'}
                                        dataSource={anomaliesData} dataEvent={anomaliesEvent} />
                                </>}
                        </div>
                    </div>
                </Masterpage>
            );
        }
    }

export default AnomaliesDetect;
