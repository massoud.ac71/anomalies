import React, { Component } from 'react';
import Masterpage from './../../layout/Masterpage';
import './registerData.scss'
import CollapseForms from './../../component/CollapseMenu/collapseMenu';
import Handler from './../../utils/Handler';
import { getMethod, postMethod } from '../../services/RestApi';
import Apis from './../../data/Api';
import { CustomErrorHandler } from '../../services/CustomErrorHander';
import LineSelectedChart from '../../component/ReactChart/LineChart/lineChartRange';
import { SpinnerHeartLine } from '../../component/SpinLoader/spinner';
import SnackBar from "../../utils/Toast";
import CRUD from './../../utils/CRUD';
import DynamicForm from './../../utils/Form-Generator';
import { Modal, ModalWrapper, ModalHeader, ModalContent, ModalFooter } from './../../component/Modal/ModalCmp';
import Confirm from "../../utils/Confirm";

// metric filed name static
const METRIC_FIELD = "Name";


class RegisterData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // items form for get mertics values form backend .
            items: [
                {
                    type: "Unit-select", field: "Name", label: "نام متریک", value: "", required: true,
                    items: [], mapping: { text: 'name', value: 'id' }
                },
                { type: "date-time-picker", field: "From", label: "از تاریخ", value: "", required: true, timer: true },
                { type: "date-time-picker", field: "To", label: "تا تاریخ", value: "", required: true, timer: true },
                { type: "number", field: "IntervalSeconds", label: "بازه زمانی(ثانیه)", value: "", required: true },
                { type: "number", field: "Mean", label: "میانگین", value: "", required: true },
                { type: "number", field: "Std", label: "انحراف معیار", value: "", required: true },
                { type: "number", field: "PeriodicityMinutes", label: "دوره تناوب(دقیقه)", value: "", required: true },
            ],
            // register form for traindel final models .
            registerForms: [
                { type: "number", field: "Epochs", label: "تعداد دوره های آموزش", value: "", required: true },
                { type: "number", field: "WindowSize", label: "اندازه پنجره", value: "", required: true },
            ],
            // anomalies form use in modal after crop data from chart and set anomalies type .
            anomaliesForm: [
                { type: "checkbox", field: "Holiday", label: "تعطیلات", value: "", required: false },
                { type: "checkbox", field: "WeekEnd", label: "آخر هفته", value: "", required: false },
                { type: "checkbox", field: "SpecialDate", label: "ایام خاص", value: "", required: false },
            ],
            // preloader
            loader: false,
            // chart data
            data: null,
            // temp crop data form chart
            cropData: null,
            // save crop data form temp
            singleCrop: null,
            // active or disable range selector from chart
            rangeSelector: false,
            // push all crop data from chart fill with single chart state
            allCrop: [],
            // value for input form for new metric
            customMetric: "",
            // show and disbale modal form
            show: false,
            // radio button anomalies type => 0 is anomalies  1 doesn't anomalies .
            isAnomaly: 0,
            // watch all crop data list bottom page .
            showPreView: false,
            // state for get all crop type => anomalies or doesn't anomalies  and from , to date and anomlies type holiday , weekend , specialday .
            trainedModel: {
                Anomalies: [],
                Normals: []
            }
        }
    }
    // life cycle hooks method for call function and get data from backend.
    componentDidMount = () => {
        this.getMetrics();
    }
    // get all metric from backend get method .
    getMetrics = async () => {
        Handler.apiHandler(await getMethod(Apis.getmetrics), (response, status) => {
            if (response?.value?.statusCode === 200 && status === 200) {
                const result = response.value.dtos
                const items = [...this.state.items];
                const res = items.map((els,index)=>{
                     if (els.field === METRIC_FIELD) {
                                for (let i = 0; i < result.length; i++) {
                                    els.items.push({id:i+1 , name:result[i]});
                                }
                                return els;
                            }
                     else return  els;
                })
                this.setState({items : res})
            } else if (status === 200 && response?.value?.statusCode !== 200) {
                CustomErrorHandler(response);
            }
        })
    }
    // change items value for input and select form .
    handleChangeItem = (field, item, e) => {
        const items = [...this.state.items];
        items.forEach(n => {
            if (n.field === field) {
                n.value = e
            }
        })
        this.setState({ items })
    }
    // change register value input .
    changeRegister = (field, item, e) => {
        const registerForms = [...this.state.registerForms];
        registerForms.forEach(n => {
            if (n.field === field) {
                n.value = e
            }
        })
        this.setState({ registerForms })
    }
    // set check box value for holiday , specialday , weekend ==> true or false .
    handleCheckbox = (field, e) => {
        let anomaliesForm = [...this.state.anomaliesForm];
        anomaliesForm = anomaliesForm.map(el => {
            if (el.field === field) {
                return {
                    ...el,
                    value: e
                }
            } else return el
        })
        this.setState({ anomaliesForm })
    }
    // save new custom metric and push on items state .
    handleAddMetric = () => {
        const { items, customMetric } = this.state;
        if (customMetric !== "") {
            const item = [...items];
            items.forEach((el) => {
                if (el.field === METRIC_FIELD) {
                    const index = el.items.length === 0 ? 1 : el.items[el.items.length - 1].id + 1
                    el.value = { id: index, name: customMetric }
                    el.items.push({ id: index, name: customMetric })
                }
            })
            this.setState({ items: item, customMetric: "" })
            SnackBar.ToastNotify("متریک جدید با موفقیت اضافه شد", "A");
        }
    }
    // change value input for custom metric .
    handleNewMetric = (field, e) => {
        this.setState({ customMetric: e })
    }
    // set radio btn for anomalies and doesn't anomalies .
    handleRadio = (event) => {
        const { name } = event.target;
        if (name === "Anomalies") {
            this.setState({ isAnomaly: 0 })
        } else {
            this.setState({ isAnomaly: 1 })
        }
    }
    // Register data with metric values  from back end and get data chart .
    handleRegisterData = async () => {
        const { items } = this.state
        let isValid = CRUD.FormValidate(items);
        if (isValid) {
            const result = CRUD.ConverterJson({ items })
            this.setState({ loader: true })
            try {
                Handler.apiHandler(await postMethod(Apis.getmetricvalues, result), (response, status) => {
                    if (response?.value?.statusCode === 200 && status === 200) {
                        setTimeout(() => {
                            const res = []
                            response.value.dtos.forEach(els=>{
                                res.push({x:new Date(els.stringTime),y:els.value})
                            })
                            debugger
                            this.setState({loader: false, data: res})
                        }, 1000)
                    } else if (status === 200 && response?.value?.statusCode !== 200) {
                        CustomErrorHandler(response);
                        this.setState({loader : false});
                    }else{
                        this.setState({loader : false});
                    }
                })
            } catch (error) {
                this.setState({loader : false});
            }
           
        } else {
            SnackBar.ToastError("لطفا مقادیر اجباری راوارد کنید", "A")
        }
    }
    // Crop from chart in Temp .
    handleCrop = ({ selectedData }) => {
        setTimeout(() => {
            this.setState({ cropData: selectedData })
        }, 100)
    }
    // Show Crop Selector on Chart .
    handleRangeSelector = () => {
        const { rangeSelector } = this.state;
        if (rangeSelector === true) {
            this.setState({ cropData: null })
        }
        this.setState({ rangeSelector: !this.state.rangeSelector })
    }
    // Save Crop data from Chart .
    save = () => {
        const { cropData } = this.state
        this.setState(prev => {
            return {
                ...prev,
                singleCrop: cropData,
                show: true
            }
        })
    }
    // Save Trained Final Model .
    saveRegister = async () => {
        const { items, trainedModel, registerForms } = this.state;
        const itemData = CRUD.ConverterJson({ items });
        const registerData = CRUD.ConverterJson({ items: registerForms })
        const { Name, From, To } = itemData
        const request = { Name, From, To, ...trainedModel, ...registerData };
        try {
            Handler.apiHandler(await postMethod(Apis.registerdata, request), (response, status) => {
                if (response.value.statusCode === 200 && status === 200) {
                    SnackBar.ToastSuccess("عملیات با موفقیت انجام شد");
                }
                else if (response.value.statusCode !== 200 && status === 200) {
                    CustomErrorHandler(response)
                }
            })
        } catch {

        }
    }
    // visible and invisibility Preview crop save data .
    handlePreView = () => {
        this.setState({ showPreView: !this.state.showPreView })
    }
    // set Anomalies and anomalies type after submit modal form .
    handleSubmitRadio = () => {
        const { singleCrop, isAnomaly , anomaliesForm } = this.state;
        const trainedModel = { ...this.state.trainedModel }
        const start = singleCrop[0].x;
        const end = singleCrop[singleCrop.length - 1].x
        const res = CRUD.ConverterJson({ items: anomaliesForm })
        if (isAnomaly === 0) {
            trainedModel[`Anomalies`].push({ From: start, To: end , Tags: {...res} })
        } else {
            trainedModel[`Normals`].push({ From: start, To: end, Tags:{...res} })
        }
        debugger
        anomaliesForm.forEach(el => el.value = false)
        this.setState({ show: false, trainedModel , anomaliesForm })
        SnackBar.ToastSuccess("دیتا مورد نظر با موفقیت  انتخاب شد", "A")
    }
    //remove items from trained models depend on anomalies or normal type from list on bottom page
    handleRemove = async (data, index, type, e) => {
        const valid = await Confirm.ConfirmForm("آیا از حذف رکورد مطمئن هستید");
        debugger
        if (valid) {
            const trainedModel = this.state.trainedModel;
            switch (type) {
                case 0:
                    trainedModel[`Anomalies`] = trainedModel.Anomalies.filter((n, i) => i !== index);
                    break;
                case 1:
                    trainedModel[`Normals`] = trainedModel.Normals.filter((n, i) => i !== index);
                    break;
                default:
                    break;
            }
            this.setState({ trainedModel })
        }

    }
    // invisibility modal .
    closeModal = () => {
        this.setState({ show: false })
    }
    // jsx  anomalies and noraml radio button and anomalies type
    renderRadio = () => {
        const { isAnomaly, anomaliesForm } = this.state;
        return (
            <div className="anomalies-form">
                <div className="anomalies-radio-form">
                    <div className="form__radio-group">
                        <input type="radio" name="Anomalies" id="Anomalies-Radio" onClick={this.handleRadio}
                            className="form__radio-input" checked={isAnomaly === 0 ? true : false} />
                        <label className="form__label-radio form__radio-label" htmlFor="Anomalies-Radio">
                            <span className="form__radio-button"></span>
                            <span style={{ marginRight: "5px" }}>ناهنجاری</span>
                        </label>
                    </div>
                    <div className="form__radio-group">
                        <input type="radio" name="Holiday" id="Holiday-Radio" onClick={this.handleRadio}
                            className="form__radio-input" checked={isAnomaly === 1 ? true : false} />
                        <label className="form__label-radio form__radio-label" htmlFor="Holiday-Radio">
                            <span className="form__radio-button"></span>
                            <span style={{ marginRight: "5px" }}>هنجار</span>
                        </label>
                    </div>
                </div>
                <div className="anomalies-check-form">
                    <DynamicForm item={anomaliesForm} changeHandler={this.handleCheckbox} />
                </div>
            </div>
        )
    }
    // jsx for modal forms 
    showModal = () => {
        return (
            <Modal zIndex={10}>
                <ModalWrapper heigth={300} width={400} marginTop={4} zIndex={11}>
                    <ModalHeader heigth={50} bgColor={"var(--Basic)"} textColor={"var(--white-color)"}>
                        <div>
                            <span>
                                نوع هنجار و ناهنجاری
                            </span>
                        </div>
                    </ModalHeader>
                    <ModalContent
                        heigth={200}
                        bgColor={"var(--white-color)"}>
                        {this.renderRadio()}
                    </ModalContent>
                    <ModalFooter
                        heigth={50}
                        bgColor={"var(--Basic)"}
                        textColor={"var(--Basic)"}
                        btnBg={"var(--white-color)"}
                        btnHover={"var(--BasicHover)"}
                        textColorHover={"var(--white-color)"}
                    >
                        <button onClick={this.handleSubmitRadio}>
                            <span style={{
                                marginLeft: "8px",
                                fontSize: "16px"
                            }}><i className="fa fa-floppy-o"></i></span>
                            <span>ذخیره</span>
                        </button>
                        <button onClick={this.closeModal}>
                            <span
                                style={{
                                    marginLeft: "8px",
                                    fontSize: "16px"
                                }}><i className="fa fa-close"></i></span>
                            <span>انصراف</span>
                        </button>
                    </ModalFooter>
                </ModalWrapper>
            </Modal>
        )
    }
    // jsx for render items in list  for saved crop data .
    renderCropDetail = (els, index, type) => {
        return (
            <div key={index} onClick={this.handleRemove.bind(this, els, index, type)}>
                <div>
                    <span>از تاریخ : </span>
                    <span>{CRUD.ConvertDateToMoment(els.From)}</span>
                    <span>   </span>
                    <span>تا تاریخ :</span>
                    <span>{CRUD.ConvertDateToMoment(els.To)}</span>
                </div>
                <div>
                    <span><i className="mdi mdi-close-box" /></span>
                </div>
            </div>
        )
    }
    //jsx  for render list for saved crop data 
    renderCrop = () => {
        const { Normals, Anomalies } = this.state.trainedModel;
        return (
            <div className="preview-crop">
                <div className="inner-preview">
                    <div className="anomalies">
                        <div className="title">ناهنجاری</div>
                        <div className="register-card">
                            {Anomalies && Anomalies.map((els, index) => this.renderCropDetail(els, index, 0))}
                        </div>
                    </div>
                    <div className="holiday">
                        <div className="title">هنجار</div>
                        <div className="register-card">
                            {Normals && Normals.map((els, index) => this.renderCropDetail(els, index, 1))}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    // jsx main-render page
    render() {
        const {
            show,
            items,
            data,
            rangeSelector,
            loader,
            cropData,
            infinityMode,
            customMetric, showPreView,
            registerForms
        } = this.state;

        return (
            <Masterpage>
                {show && this.showModal()}
                <div className="register-data">
                    <CollapseForms
                        add={true}
                        addMetric={this.handleAddMetric}
                        changeAddMetric={this.handleNewMetric}
                        customMetric={customMetric}
                        items={items}
                        Change={this.handleChangeItem}
                        register={this.handleRegisterData}
                        txtBtn={"مشاهده"} />
                    <div className="register-chart">
                        {data === null && loader === true && <SpinnerHeartLine />}
                        {data !== null && loader === false &&
                            <>
                                <LineSelectedChart
                                    id={"RegisterData"}
                                    data={data}
                                    rangeSelector={rangeSelector}
                                    crop={cropData}
                                    handleSave={this.save}
                                    ShowPreView={showPreView}
                                    // handlePrint={this.handlePrint}
                                    handleRangeSelector={this.handleRangeSelector}
                                    handlePreView={this.handlePreView}
                                    handleInfinity={false}
                                    handleChange={this.handleCrop}
                                    infinityModeIsActive={infinityMode}
                                    lineColor={"#DC1432"}
                                    labelFormat={"y/M/d"}
                                    labelColor={"#132743"}
                                    tooltips={{
                                        display: "Always",
                                        fill: "#132743",
                                        format: "y/M/d",
                                        textStyle: {
                                            color: "#FFDB44"
                                        }
                                    }}
                                    rangeSelectorActivate={false}
                                    navigator={{
                                        type: "Retangle",
                                        borderColor: "#132743",
                                        borderWidth: 1
                                    }}
                                    minimum={0}
                                    maximum={500}
                                    interval={1}
                                    ChartWidth={"100%"}
                                    ChartHeight={"400px"}
                                    primaryXshow={true}
                                    primaryYshow={false}

                                />
                                <div className="register-form">
                                    <div className="register-inner-form">
                                        <DynamicForm item={registerForms} changeHandler={this.changeRegister} />
                                    </div>
                                    <div className="register-inner-btn" onClick={this.saveRegister}>
                                        <button>
                                            <span><i className="mdi mdi-plus" /></span>
                                            <span>آموزش مدل</span>
                                        </button>
                                    </div>
                                </div>
                                {showPreView && this.renderCrop()}
                            </>
                        }
                    </div>
                </div>
            </Masterpage>
        );
    }
}

export default RegisterData
