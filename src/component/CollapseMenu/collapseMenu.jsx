import './collapseMenu.scss'
import DynamicForm from '../../utils/Form-Generator';
import { useState, } from 'react';
import { Input } from '../Form'
import { SpinnerHeartLine } from "../SpinLoader/spinner";

const CollapseForms = ({
    items, Change, register, txtBtn, add, addMetric, changeAddMetric, customMetric
    , scenarioItems, changeSenario, divider
}) => {
    const [toggle, setToggle] = useState(false)
    const [toggleMetric, setToggleMetric] = useState(false);
    const handleToggle = () => {
        setToggleMetric(false);
        setToggle(prev => !prev);
    }
    const handleToggleMetric = () => {
        setToggleMetric(prev => !prev);
    }
    const addNewMetric = () => {
        setToggleMetric(false);
        addMetric();
    }
    return (
        <div className={toggle ? "form-gen-collapse active" : "form-gen-collapse"}>
            <ul className=" hamburger-btn-inner" onClick={handleToggle}>
                <li className=" lines" />
                <li className=" lines" />
                <li className=" lines" />
            </ul>
            <button className="register-btn" onClick={register}>
                <span>{txtBtn}</span>
            </button>
            {add &&
                <>
                    <button className={toggleMetric ? "add-btn active" : "add-btn"} onClick={handleToggleMetric}>
                        <span><i className="fa fa-plus" /></span>
                        <span>افزودن متریک جدید</span>
                    </button>
                    <div className={toggleMetric ? "custom-metric active" : "custom-metric"}>
                        <Input
                            type={"text"}
                            required={false}
                            label={"متریک جدید"}
                            value={customMetric}
                            change={changeAddMetric.bind(this, "MetricName")}
                        />
                        <div onClick={addNewMetric}>
                            <button className="submit-new-metric">
                                <span><i className="fa fa-save" /></span>
                            </button>
                        </div>
                    </div>
                </>
            }
            <div className="collapse-form">
                <div className="inner-form">
                    <DynamicForm item={items} changeHandler={Change} />
                </div>
                {divider &&
                    <>
                        <div className="title">
                            <span>شبیه ساز ناهنجاری</span>
                        </div>
                        <div className="inner-form">
                            <DynamicForm item={scenarioItems} changeHandler={changeSenario} />
                        </div>
                    </>}
                <a href="#gotoBottom" className="button-scrolling">
                    <span><i className="mdi mdi-chevron-double-down"/></span>
                </a>
                <div id="gotoBottom"></div>
            </div>

        </div>
    );
}

export default CollapseForms;
