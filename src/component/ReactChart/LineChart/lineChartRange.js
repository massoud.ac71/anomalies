import React, {useState, useRef} from 'react'
import {
    LineSeries,
    DateTime,
    Inject,
    RangeNavigatorComponent,
    RangenavigatorSeriesCollectionDirective,
    RangenavigatorSeriesDirective,
    RangeTooltip,
    ChartComponent,
    Legend,
    Tooltip,
    DataLabel,
    Category,
    SeriesCollectionDirective,
    SeriesDirective,
} from '@syncfusion/ej2-react-charts';

import './lineChart.scss';

const LineSelectedChart = ({
                               id,
                               data,
                               crop,
                               ShowPreView,
                               infinityModeIsActive,
                               lineColor,
                               labelFormat,
                               tooltips,
                               minimum,
                               maximum,
                               interval,
                               ChartWidth,
                               ChartHeight,
                               primaryXshow,
                               primaryYshow,
                               navigator,
                               labelColor,
                               rangeSelector,
                               handleSave,
                               handlePrint,
                               handleRangeSelector,
                               handlePreView,
                               handleInfinity,
                               handleChange
                           }) => {
    const tooltip = {
        enable: rangeSelector,
        displayMode: tooltips.display,
        fill: tooltips.fill,
        options: 0.8,
        textStyle: {fontFamily: "IranSans", color: tooltips.textStyle.color, size: "14px"},
        format: labelFormat
    };
    const navigatorStyleSetting = {
        thumb: {
            type: navigator.type, // Circle
            border: {width: navigator.borderWidth, color: navigator.borderColor},
        },
    }
    const border = {width: 1, color: "#132743", opacity: 0.6};
    const labelStyle = {
        color: labelColor,
        fontFamily: "IranSans",
        size: "12px",
        textAlignment: "center",
        textOverflow: 'ellipsis',
    }
    return (
        <div style={{direction: "ltr", textAlign: "right", width: "100%", height: "470px"}}>
            {handleSave && <button className="btnChartLine"
                                   onClick={handleSave}
                                   disabled={!rangeSelector}>
                        <span>
                        <i className="fa fa-save"/>
                        </span></button>
            }
            {
                /* {handlePrint && <button className="btnChartLine " onClick={() => handlePrint(exportFile)}>
                                <span>
                                    <i className="fa fa-print" aria-label="hidden"></i>
                                </span>
                            </button>} */
            }
            {
                handleInfinity &&
                <button className={infinityModeIsActive ? "btnChartLine active" : "btnChartLine"}
                        onClick={handleInfinity}>
            <span>
            <i className="mdi mdi-infinity" aria-label="hidden"/>
            </span></button>
            }
            {
                handleRangeSelector &&
                <button className={rangeSelector ? "btnChartLine active" : "btnChartLine"}
                        onClick={handleRangeSelector}>
            <span>
            <i className="mdi mdi-crop-free" aria-label="hidden"/>
            </span></button>
            } {
            handlePreView && <button className={ShowPreView ? "btnChartLine active" : "btnChartLine"}
                                     onClick={handlePreView}
                                     disabled={!rangeSelector}>
            <span>
            <i className="fa fa-eye"
               aria-label="hidden"/>
            </span></button>
        } {
            !rangeSelector && <ChartComponent
                id={`${id}WithoutSelector`}
                width={ChartWidth}
                height={ChartHeight}
                allowExport={true}
                enableRtl={false}
                tooltip={
                    {enable: true}
                }
                enablePersistence={true}
                primaryXAxis={
                    {
                        visible: true,
                        // minimum: data.length[0],
                        // maximum: data.length - 1,
                        // interval: "Days",
                        // intervalType: "Days",
                        labelFormat: labelFormat,
                        valueType: "DateTime",
                        labelStyle: labelStyle,
                        intervalType: "Auto"
                    }
                }
                primaryYAxis={
                    {
                        visible: primaryYshow,
                        labelStyle: labelStyle,
                        tooltips: {
                            enabled: true
                        }
                    }
                }
                margin={
                    {
                        bottom: 5,
                        left: 20,
                        top: 5,
                        right: 18
                    }
                }
                majorTickLines={
                    {
                        color: "var(--violet-color-dark)",
                        height: 5,

                    }
                }
                intervalType={"Months"}>
                <
                    Inject services={
                    [LineSeries, DateTime, Legend, Tooltip, DataLabel, Category]
                }
                /> <SeriesCollectionDirective>
                <SeriesDirective dataSource={data}
                                 xName="x"
                                 yName="y"
                                 width={3}
                                 type={"Line"}
                                 fill={lineColor}
                                 toolTip={true}>
                </SeriesDirective>
            </SeriesCollectionDirective>
            </ChartComponent>
        } {
            rangeSelector && <RangeNavigatorComponent
                id={`${id}WithSelector`}
                labelPosition={"Outside"}
                valueType={"DateTime"}
                labelFormat="y/M/d"
                // value={[new Date(data[0]),new Date(data[data.length-1])]}
                labelIntersectAction="Hide"
                tooltip={tooltip}
                width="100%"
                height="400px"
                changed={handleChange}
                enableRtl={false}
                navigatorStyleSettings={navigatorStyleSetting}
                allowSnapping={false}
                enablePersistence={true}
                animationDuration={1500}
                labelStyle={labelStyle}
                majorGridLines={
                    {
                        color: "#e3e3e3",
                        width: 1,
                    }
                }
                resized={
                    (e) => {

                    }
                }
                labelRender={
                    e => {

                    }
                }
                useGroupingSeparator={true}
                tickPosition={"Inside"}
                skeletonType={"DateTime"}
                series={
                    {
                        fill: "red"
                    }
                }
                margin={
                    {
                        bottom: 5,
                        left: 8,
                        top: 5,
                        right: 8
                    }
                }
                majorTickLines={
                    {
                        color: "var(--violet-color-dark)",
                        height: 5,
                    }
                }
                // maximum={data[data.length-1]}
                // minimum={data[0]}
                interval={interval}
                intervalType={"Months"}
                disableRangeSelector={false}>
                <
                    Inject services={
                    [LineSeries, DateTime, RangeTooltip]
                }
                /> <RangenavigatorSeriesCollectionDirective>
                <RangenavigatorSeriesDirective
                    dataSource={data}
                    xName='x'
                    yName='y'
                    type='Line'
                    width={3}
                    fill={lineColor}
                    // border={{color: "pink" ,width:"2px"}}
                />
            </RangenavigatorSeriesCollectionDirective>
            </RangeNavigatorComponent>
        }
            {/*{*/}
            {/*    ShowPreView && <div style={*/}
            {/*        {*/}
            {/*            width: "100%",*/}
            {/*            height: "calc(100vh - 60px)",*/}
            {/*            overflow: "hidden",*/}
            {/*            overflowY: "auto",*/}
            {/*            marginTop: "50px"*/}
            {/*        }}>*/}
            {/*        {crop !== null && crop.map((node, i) => {*/}
            {/*            return*/}
            {/*            <div key={i}*/}
            {/*                        className="chartline_crop">*/}
            {/*                <div><span> {`X-Value :   ${node.x}`} </span></div>*/}
            {/*                <div><span> {`Y-Value :   ${node.y}`} </span></div>*/}
            {/*            </div>*/}
            {/*        })*/}
            {/*    } </div>}*/}
        </div>
    )
}
export default LineSelectedChart;