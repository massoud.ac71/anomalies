import React, { useState, useEffect } from "react";
import { ChartComponent, SeriesCollectionDirective, SeriesDirective, Inject, Tooltip, AnnotationsDirective, AnnotationDirective, DateTime, MultiColoredLineSeries, ChartAnnotation, SegmentsDirective, SegmentDirective } from '@syncfusion/ej2-react-charts';
import PropTypes from 'prop-types';

let dataValues = [];
[
    380, 410, 310, 540, 510, 330, 490, 470, 472, 460, 550, 420, 380, 430, 385, 520, 580, 420, 350, 505,
    535, 410, 204, 400, 415, 408, 415, 350, 375, 500, 390, 450, 440, 350, 400, 365, 490, 400, 520, 510,
].map((value, index) => {
    dataValues.push({ Index: index, XValue: new Date(1800 + index, 0, 1), YValue: value });
});
const LineChartZoneColor = ({
    handleInfinityMode, segment, primaryX, primaryY, tooltip, width, height,
    seriesValueText, infinityMode
}) => {
    const [dataSource, setDataSource] = useState(dataValues)
    const [segments, setSegment] = useState([{ value: 150, color: '#ffdb44' },
    { value: 300, color: '#22119d' },
    { value: 450, color: '#15b022' },
    { value: 500, color: '#db1d4b' },
    { value: 700, color: '#067eed' },
    { color: '#132743' }])
    useEffect(() => {
        const timer = setInterval(() => {
            let index = dataSource[dataSource.length - 1]
            const newValue = {
                Index: index.Index + 1,
                XValue: new Date(1800 + index.Index, 0, 1),
                YValue: Math.floor((Math.random() * 1000) * 950) / 1000
            }
            if (!infinityMode) {
                if (dataSource.length <= 200) {
                    setDataSource(old => [...old, newValue])
                }
                if (dataSource.length >= 200) {
                    clearInterval(timer)
                }
            }
            else if (infinityMode) {

                if (dataSource.length <= 200) {
                    setDataSource(old => [...old, newValue])
                }
                else if (dataSource.length >= 200 + 1) {
                    setDataSource(old => {
                        old.shift()
                        return [...old, newValue]
                    })
                }
            }
        }, 100)
        return () => {
            clearInterval(timer)
        }
    })
    useEffect(() => {
        if (segment === null) setSegment([{ value: 150, color: '#ffdb44' },
        { value: 300, color: '#22119d' },
        { value: 450, color: '#15b022' },
        { value: 500, color: '#db1d4b' },
        { value: 700, color: '#067eed' },
        { color: '#132743' }])
        else {
            const { Segment, Color } = segment;
            if (Segment.length !== Color.length) {
                Segment.shift()
            }
            let newSegments = []
            for (let i = 0; i < segment.Color.length; i++) {
                newSegments.push({
                    color: Color[i],
                    value: Segment[i]
                })
            }
            setSegment(newSegments)
        }
        return () => {
        }
    }, [segment])
    const handleToolTip = () => {
        return {
            enable: tooltip.enable,
            shared: tooltip.shared,
            duration: tooltip.duration,
            fadeOutDuration: tooltip.fadeOutDuration,
            enableAnimation: tooltip.enableAnimation,
            enableMarker: tooltip.enableMarker,
            fill: tooltip.fill,
            textStyle: {
                color: tooltip.textStyleColor,
                fontFamily: "IranSans",
                textAlignment: "Center",
                textOverflow: "Wrap"
            }
        }
    }
    return (<div className='control-pane'>
        {handleInfinityMode && <button className={handleInfinityMode ? "btnChartLine active" : "btnChartLine"} onClick={handleInfinityMode}>
            <span>
                <i className="mdi mdi-infinity" aria-label="hidden"></i>
            </span>
        </button>}
        <div className='control-section'>
            <ChartComponent
                id='charts'
                style={{ textAlign: "center" }}
                primaryXAxis={{
                    valueType: primaryX.valueTypeX,
                    labelFormat: primaryX.lableFormatX,
                    intervalType: primaryX.intervalTypeX,
                    edgeLabelPlacement: 'None',
                    enableAutoIntervalOnZooming: true,
                    enableScrollbarOnZooming: true,
                    majorGridLines: primaryX.majorGridLinesX,
                    labelStyle: {
                        fontFamily: "IranSans",
                        size: "12px",
                        color: primaryX.lableStyleXColor,
                        textOverflow: "Wrap",
                        textAlignment: "Center"
                    },
                    maximumLabelWidth: 1,
                    maximumLabels: 3,
                    minorTickLines: primaryX.minorTickLinesX,
                    majorTickLines: primaryX.majorTickLinesX,
                    minorTicksPerInterval: primaryX.minorTicksPerIntervalX,
                    opposedPosition: primaryX.opposedPositionX,
                    plotOffset: primaryX.plotOffsetX,
                    rangePadding: primaryX.rangePaddingX,
                    scrollbarSettings: {
                        enable: true,
                        pointsLength: 1,
                    },
                    startFromZero: true,
                    tickPosition: primaryX.tickPositionX,
                    title: primaryX.titleX,
                    titleStyle: {
                        color: primaryX.textStyleColorX,
                        fontFamily: "IranSans",
                        size: "14px",
                        textAlignment: "Center",
                        textOverflow: "Trim",
                    },
                    visible: primaryX.visibleX,

                }}
                primaryYAxis={{
                    lineStyle: { width: 0 },
                    labelFormat: `{value}${primaryY.valueTypeY}`,
                    rangePadding: 'None',
                    enableAutoIntervalOnZooming: true,
                    enableScrollbarOnZooming: true,
                    minimum: primaryY.minimumY,
                    maximum: primaryY.maximumY,
                    interval: primaryY.interval,
                    // lineStyle: { width: 1 },
                    majorTickLines: primaryY.minorTickLinesY,
                    minorTickLines: primaryY.majorTickLinesY,
                    majorGridLines: { width: primaryY.majorGridLinesY },
                    labelStyle: {
                        fontFamily: "IranSans",
                        size: "12px",
                        color: primaryY.lableStyleColorY,
                        textOverflow: "Wrap",
                        textAlignment: "Center"
                    },
                    maximumLabelWidth: 1,
                    maximumLabels: 3,
                    minorTicksPerInterval: primaryY.minorTicksPerIntervalY,
                    opposedPosition: primaryY.opposedPositionY,
                    plotOffset: primaryY.plotOffsetY,
                    // scrollbarSettings: {
                    //     enable: true,
                    //     pointsLength: 1,
                    //     range: {
                    //         maximum: "2500",
                    //         minimum: "1900",
                    //     }
                    // },
                    startFromZero: true,
                    tickPosition: primaryY.tickPositionY,
                    title: primaryY.titleY,
                    titleStyle: {
                        color: primaryY.textStyleColorY,
                        fontFamily: "IranSans",
                        size: "14px",
                        textAlignment: "Center",
                        textOverflow: "Trim",
                    },
                    visible: primaryY.visibleY,
                }}
                tooltip={handleToolTip()}
                legendSettings={{ visible: false }}
                chartArea={{ border: { width: 0 } }}
                // load={onLoad.bind(this)}
                width={`${width}%`}
                height={`${height}px`}
                title={"اندازه گیری میزان ورودی دیتا"}
            // loaded={onChartLoad}
            >
                <Inject services={[MultiColoredLineSeries, ChartAnnotation, DateTime, Tooltip]} />
                {/* <AnnotationsDirective>
                    <AnnotationDirective content={content} region='Series' x='90%' y='12%'></AnnotationDirective>
                </AnnotationsDirective> */}
                <SeriesCollectionDirective>
                    <SeriesDirective dataSource={dataSource} xName='XValue' yName='YValue' name={seriesValueText} type='MultiColoredLine' segmentAxis='Y' >
                        <SegmentsDirective>
                            {segments !== null && segments.map((n, i) => {

                                return (
                                    <SegmentDirective key={i} value={n?.value ? n.value : null} color={n.color}></SegmentDirective>
                                )
                            })}
                            {/* <SegmentDirective value={450} color='red'></SegmentDirective>
                            <SegmentDirective value={500} color='green'></SegmentDirective>
                            <SegmentDirective color='blue'></SegmentDirective> */}
                        </SegmentsDirective>
                    </SeriesDirective>
                </SeriesCollectionDirective>
            </ChartComponent>
        </div>
    </div>
    );
}
export default LineChartZoneColor;