import {
    StockChartComponent,
    StockChartSeriesCollectionDirective,
    StockChartSeriesDirective,
    Inject,
    Tooltip,
    DateTime,
    RangeTooltip,
    SplineSeries,
    CandleSeries,
    HiloOpenCloseSeries,
    HiloSeries,
    RangeAreaSeries,
    Trendlines,
    Crosshair,
    EmaIndicator,
    RsiIndicator,
    BollingerBands,
    TmaIndicator,
    MomentumIndicator,
    SmaIndicator,
    AtrIndicator,
    AccumulationDistributionIndicator,
    MacdIndicator,
    StochasticIndicator,
    Export,
    LineSeries,
    StepLineSeries
} from '@syncfusion/ej2-react-charts';
import CRUD from '../../../utils/CRUD';
// import {useEffect, useState} from "react";
import React, { useState, useEffect } from "react";
// class StockEventsChart extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             data: null
//         }
//         this.chart = null
//     }
//     componentDidMount = () => {
//         const { dataSource } = this.props
//         console.log("DidMount")
//         this.setState({ data: dataSource })
//     }
//     shouldComponentUpdate = (prev, next) => {
//         if (prev.dataSource.length !== next.data.length) {
//             console.log("Updated")
//             // console.log(`Before : ${this.chart.stockChart.series[0].dataSource.length}`)
//             debugger
//             // this.chart.stockChart.series[0].dataSource = prev.dataSource;
//             // console.log(`After : ${this.chart.stockChart.series[0].dataSource.length}`)
//             if (this.chart !== null) {
//                 this.chart.stockChart.refresh();
//             }
//             this.setState({ data: prev.dataSource })
//             return true;
//         } else {
//             return false;
//         }

//     }
const StockEventChart = ({ dataSource, dataEvent }) => {
    const [data, setData] = useState(dataSource);
    const [eventData, setEventData] = useState(dataEvent);
    const [chartsEvent , serChart] = useState(null);
    useEffect(() => {
        setData(prev => dataSource);
        setEventData(prev => dataEvent);
        if (chartsEvent !== null) {
            chartsEvent.stockChart.refresh();
        }
        return () => {

        }
    }, [dataSource , dataEvent])

    const tooltipRender = (args) => {
        if (args.text.split('<br/>')[4]) {
            // let target = parseInt(args.text.split('<br/>')[4].split('<b>')[1].split('</b>')[0]);
            // let value = (target / 100000000).toFixed(1) + 'B';
            // args.text = args.text.replace(args.text.split('<br/>')[4].split('<b>')[1].split('</b>')[0], value);
            args.text = `<span>مقدار : <b>${args.point.y}</b></span><br/><span>تاریخ : <b>${CRUD.ConvertDateToMoment(args.point.x)}</b></span>`
        }
    }
    const renderLoadBeforeStock = (args) => {
        debugger
        serChart(args)
    }
    // const renderLoadAfterStock = (args) => {
    //     this.chart = args
    // }
    const LABEL_STYLE = {
        color: "#000000",
        fontFamily: 'IranSans',
        size: "12px",
        textAlignment: "center",
    }
    const primaryXAxis = {
        valueType: 'DateTime', majorGridLines: { color: 'transparent' },
        labelStyle: LABEL_STYLE, crosshairTooltip: { enable: true },
        labelPosition: "outside",
        intervalType: "Seconds"
    };
    const primaryYAxis = {
        labelStyle: {
            color: "#000000",
            fontFamily: 'IranSans',
            size: "12px",
            textAlignment: "center",
            valueType: "Double",
        },
        crosshairTooltip: { enable: true },
        labelPosition: "outside",
        labelFormat : "n3",

    };
    const renderToolTip = {
        enable: true, fill: "var(--Basic)",
        shared: false, textStyle: { color: "var(--Text)", fontFamily: "IranSans" }
    }
    return (
        <div style={{ direction: "ltr" , fontFamily:"IranSans" }}>
            <StockChartComponent
                id='Stock-Custom-Charts'
                primaryXAxis={primaryXAxis}
                primaryYAxis={primaryYAxis}
                title={"Anomalies Detecteds"}
                crosshair={{ enable: true }}
                chartArea={{ border: { width: 3 } }}
                stockEvents={eventData}
                // width="100%"
                height='500px'
                width="100%"
                enablePeriodSelector={false}
                isSelect={true}
                selectionMode="DragXY"
                // load={renderLoader}
                tooltipRender={tooltipRender}
                tooltip={renderToolTip}
                isTransposed={true}
                // enableCustomRange={true}
                enableSelector={true}
                enablePersistence={true}
                load={renderLoadBeforeStock}
                // loaded={renderLoadAfterStock}
                immediateRender={true}
                onZooming={true}
                allowIntervalData={true}
                allowSnapping={true}
            >
                <Inject
                    services={[DateTime, Tooltip, LineSeries, StepLineSeries, RangeTooltip, Crosshair, SplineSeries, CandleSeries, HiloOpenCloseSeries, HiloSeries, RangeAreaSeries, Trendlines,
                        EmaIndicator, RsiIndicator, BollingerBands, TmaIndicator, MomentumIndicator, SmaIndicator, AtrIndicator, Export,
                        AccumulationDistributionIndicator, MacdIndicator, StochasticIndicator]} />
                <StockChartSeriesCollectionDirective>
                    <StockChartSeriesDirective dataSource={data} type='Spline' xName='x' width={3} yName='high'
                        close='high'
                        marker={{ visible: true, fill: "#FFFFFF", shape: "Pentagon" }}
                        enableTooltip={true}
                        animation={{ enable: true }}

                    >
                    </StockChartSeriesDirective>
                </StockChartSeriesCollectionDirective>
            </StockChartComponent>
        </div>
    );
}
export default StockEventChart;