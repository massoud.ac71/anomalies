import React from 'react'
import { Helmet } from 'react-helmet-async';

const CustomHelmet = ({ text }) => {
    return (
        <Helmet>
            <meta charSet="utf-8" />
            <title>{`سامانه بینا | ${text}`}</title>
        </Helmet>
    );
}
export default CustomHelmet;