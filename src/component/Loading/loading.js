import React, { memo } from 'react'
import loadingImage from './loading.svg';

const style = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    userSelect: 'none',
    background: 'none',
    zIndex: 800000
}
const Loading = () => {
    return (
        <div style={style}>
            <img src={loadingImage} alt="Loading" />
                در حال بارگزاری
        </div>
    );
}
export default memo(Loading);
