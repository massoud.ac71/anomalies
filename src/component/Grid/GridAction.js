import React, { Component, createRef, Fragment } from 'react';
import './Grid.css';

import Loading from '../Loading/loading'
import Paginate from './Paginate';
import GridContext from './GridContext';
import $ from 'jquery';
import { mapStore } from '../../globalStore/GlobalStore'

class GridAction extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            pageSize: this.props.pageSize || 10,
            pageNumber: this.props.pageNumber || 1,
            dataLength: null,
            columns: this.createColumns(),

        }

        this.dom = createRef();
    }
    async componentDidMount() {
        await this.loadByQuery(() => {
            this.timer = setTimeout(() => {
                if (this.state.data && this.state.data !== null && this.state.data.length > 6) {
                    $(this.dom.current).height($(window).height() - 300)
                }
            }, 100)

        });
    }
    removeRow = async (row, index) => {
        const { remove } = this.props;
        const res = await remove(row, index + 1);
        if (res) {
            let rows = Array.from(this.state.data);
            rows.splice(index, 1);
            this.setState({
                data: rows
            })
            this.loadByQuery()
        }

    }


    createColumns = () => {
        const { columns } = this.props;

        return columns.map((o, i) => {
            return {
                showTitle: o.showTitle,
                title: o.title,
                filtering: o.filtering,
                filterType: 3,
                filterText: '',
                dataType: o.type,
                // tableName: o.tableName,
                // fieldName: o.fieldName,
                sort: { show: false, desc: false },
                search: false,
                select: false,
                sorting: o.sorting
            }
        });
    }
    reload = () => {
        const { reloaded } = this.props;
        this.loadByQuery(() => {
            reloaded()
        });
    }
    loadByQuery = async (cb) => {
        const { actions } = this.context;
        const { api, id , reload } = this.props;
        const { pageSize, pageNumber, columns } = this.state;

        const res = await api({ pageSize, pageNumber, columns });
        actions.loadGrid(id, { ...this.props, items: res })
        if(res=== null || !res)  {
            this.setState({data : null})
            return null
        }
        await cb && cb()
        this.setState({
            data: res.Items,
            dataLength: res.pageCount
        })
        // this.setState({
        //     data: res.dtos,
        //     dataLength: res.dtos.length
        // })
    }


    removeAllFilters = () => {
        let columns = Array.from(this.state.columns);
        columns.forEach(o => {
            o.filterText = '';
        })

        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });
    }

    createActions = (row, index) => {
        const { edit, gridType, editService } = this.props;
        return (
            <div className="grid-cell thin" style={{ minWidth: `${gridType ? "110px" : "70px"}` }}>
                {gridType ? (
                    <div style={{ justifyContent: "center" }}>
                        <button onClick={() => { editService(row) }}>
                            <span><i className="fa fa-cogs"></i></span>
                        </button>&nbsp;
                        <button onClick={() => { edit(row) }}>
                            <span><i className="fa fa-pencil"></i></span>
                        </button>&nbsp;
                        <button onClick={this.removeRow.bind(this, row, index)}>
                            <span><i className="fa fa-trash"></i></span>
                        </button>
                    </div>) :
                    (
                        <div style={{ display: 'flex' }}>
                            <button onClick={() => { edit(row) }}>
                                <span><i className="fa fa-pencil"></i></span>
                            </button>&nbsp;
                            <button onClick={this.removeRow.bind(this, row, index)}>
                                <span><i className="fa fa-trash"></i></span>
                            </button>
                        </div>
                    )
                }
            </div>
        )
    }


    handleSort = (i) => {
        const columns = Array.from(this.state.columns);


        if (columns[i].sort.show === false) {
            columns.forEach(o => {
                o.sort.show = false;
                o.sort.desc = false;
            });
            columns[i].sort.show = true;
        }
        else if (columns[i].sort.show === true && columns[i].sort.desc === true) {
            columns.forEach(o => {
                o.sort.show = false;
                o.sort.desc = false;
            });

        }
        else if (columns[i].sort.show === true) {
            columns[i].sort.desc = true

        }

        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });

    }

    changeFilterType(i, e) {
        const columns = Array.from(this.state.columns);
        columns[i].filterType = Number(e.target.value);
        this.setState({ columns })
    }
    changeFilterText(i, e) {
        const columns = Array.from(this.state.columns);
        columns[i].filterText = e.target.value;
        // columns[i].search = false
        this.setState(prevState => {
            return { columns }
        },
            () => {
                // columns[i].search = false
                // this.loadByQuery()
            }
        )
    }
    sendFilter = (i, e) => {
        const columns = Array.from(this.state.columns);
        columns[i].search = false;
        // columns[i].filterText = "";
        this.setState(prevState => {
            return {
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
            columns[i].filterText = "";
        });
    }
    clearFilter(i, e) {
        const columns = Array.from(this.state.columns);
        //columns[i].filterType =1;
        columns[i].filterText = '';

        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });
    }
    // renderFilters = () => {
    //     const { columns } = this.state;
    //     const cols = columns.map((o, i) => {
    //         return (
    //             <div className="grid-cell" key={i} >
    //                 <div className="filter-area">
    //                     <input placeholder="جستجو ..." value={o.filterText} onChange={this.changeFilterText.bind(this, i)} />
    //                     <div style={{ display: 'flex' }}>
    //                         <button type="button" onClick={this.sendFilter}>
    //                             <span className="icon">
    //                                 <i className="fa fa-filter"></i>
    //                             </span>
    //                         </button>
    //                         <button type="button" onClick={this.clearFilter.bind(this, i)}>
    //                             <span className="icon">
    //                                 <i className="fa fa-eraser"></i>
    //                             </span>
    //                         </button>
    //                         <select defaultValue={o.filterType} className="browser-default" onChange={this.changeFilterType.bind(this, i)}>
    //                             <option value="1">برابر است با</option>
    //                             <option value="2">برابر نیست با</option>
    //                             <option value="3">شامل</option>
    //                             <option value="4">شامل نمیشود</option>
    //                         </select>
    //                     </div>
    //                 </div>
    //             </div>
    //         )
    //     })
    //     return (
    //         <div className="grid-row" >
    //             <div className="grid-cell thin"  >
    //                 <button style={{ margin: "7px 1px" }} type="button" onClick={this.removeAllFilters}>
    //                     <span className="icon">
    //                         <i className="fa fa-eraser"></i>
    //                     </span>
    //                 </button>
    //             </div>
    //             <div className="grid-cell thin" ></div>
    //             {cols}
    //         </div>
    //     )
    // }
    firstPage = () => {
        const { pageNumber, dataLength, pageSize } = this.state;

        this.setState(prevState => {
            return {
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
        });
    }
    lastPage = () => {
        const { pageNumber, dataLength, pageSize } = this.state;

        this.setState(prevState => {
            return {
                pageNumber: Math.ceil(dataLength / Number(pageSize))
            }
        }, () => {
            this.loadByQuery()
        });
    }
    nextPage = () => {
        const { pageNumber, dataLength, pageSize } = this.state;
        if (pageNumber >= Math.ceil(dataLength / Number(pageSize))) return;

        this.setState(prevState => {
            return {
                pageNumber: prevState.pageNumber + 1
            }
        }, () => {
            this.loadByQuery()
        });
    }
    prevPage = () => {
        const { pageNumber } = this.state;
        if (pageNumber <= 1) return;

        this.setState(prevState => {
            return {
                pageNumber: prevState.pageNumber - 1
            }
        }, () => {
            this.loadByQuery()
        });

    }
    changePageSize = (size) => {
        this.setState(prevState => {
            return {
                pageSize: Number(size),
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
        });
    }

    renderRows = () => {
        const { data } = this.state;
        return data.map((o, i) => {
            return this.renderRow(o, i)
        })
    }
    renderRow = (row, index) => {
        const { columns } = this.state;
        // const { rowIndex } = this.props;
        const rows = columns.map((o, i) => {
            return (
                <div key={i} className="grid-cell" title={row[o.title]}>
                    <div className="grid-cell-text">
                        {row[o.title]}
                    </div>
                </div>)
        });
        return (
            <div className={`grid-row ${index % 2 === 0 ? "table-row-odd" : "table-row-even"}`} key={index}>
                {this.createActions(row, index)}
                <div className="grid-cell thin" style={{ padding: "8px 15px" }}>{index + 1}</div>
                {rows}
            </div>
        )
    }
    renderColumns = () => {
        const { columns } = this.state;
        const { gridType } = this.props
        const cols = columns.map((o, i) => {
            return (
                <div className="grid-cell search-input" style={{ display: "flex" }} key={i}>
                    <div  >
                        {o.showTitle}
                    </div>
                    {o.sorting && <div className="grid-sort" onClick={this.handleSort.bind(this, i)} key={i}  >
                        {o.sort.show && !o.sort.desc ?
                            (<span style={{ fontSize: 16 }}> <i className="fa fa-sort-asc"></i> </span>) :
                            o.sort.show && o.sort.desc ?
                                (<span style={{ fontSize: 16 }}> <i className="fa fa-sort-desc"></i> </span>) : (<span style={{ fontSize: 20 }}><i className="fa fa-sort" aria-hidden="true"></i></span>)}
                        {/* {o.sort.show && !o.sort.desc && <span style={{ fontSize: 20 }}>&#8679;</span>}
                        {o.sort.show && o.sort.desc && <span style={{ fontSize: 20 }}>&#8681;</span>} */}
                    </div>}
                    { o.filtering &&
                        <>
                            <span style={{ fontSize: "16px", cursor: "pointer", marginRight: "10px" }} onClick={this.handleSearchShow.bind(this, i)}><i className="fa fa-search" aria-hidden="true"></i></span>
                            <div className={o.search ? "search-input__style" : "search-display"}>
                                <input placeholder="جستجو ..." value={o.filterText} onChange={this.changeFilterText.bind(this, i)} />
                                <button className="search_btn" onClick={this.sendFilter.bind(this, i)}>
                                    <span><i className="fa fa-search"></i></span>
                                </button>
                            </div>
                        </>
                    }
                </div>
            )
        })
        return (
            <Fragment>
                <div className="grid-row table-header" >
                    <div className="grid-cell thin" style={{ padding: "8px 15px", minWidth: `${gridType ? "110px" : "70px"}` }}>عملیات</div>
                    <div className="grid-cell thin" style={{ padding: "8px 15px" }}>ردیف</div>
                    {cols}
                </div>
            </Fragment>
        )
    }
    handleSearchShow = (i, e) => {
        const columns = Array.from(this.state.columns)
        columns[i].search = columns[i].search === true ? false : true
        this.setState({ columns })
    }
    getTemplateColumns = () => {
        let str = '90px 90px ';
        const { columns } = this.state;
        columns.forEach(o => {
            str += '250px '
        });
        return str;
    }
    getTemplateRows = () => {
        let str = '85px 85px';
        const { columns } = this.state;
        columns.forEach(o => {
            str += '40px '
        });
        return str;
    }

    // sendPhoneNumber = (e) => {
    //     const { phonenumber } = this.state
    //     if (!phonenumber || phonenumber === "") return Toast.Error("لطفا شماره تلفن را وارد کنید")
    //     this.setState(prevState => {
    //         return {
    //             pageNumber: 1
    //         }
    //     }, () => {
    //         this.loadByQuery()
    //     });
    // }
    // handlephone = (event) => {
    //     this.setState({ phonenumber: event })
    // }
    render() {
        const { data } = this.state;
        const { reload } = this.props;
        if (reload) { this.reload() }
        if (data === null || !data) return (
            <Fragment>
                <div className="a-grid" >
                    <div ref={this.dom} style={{ direction: 'rtl', gridTemplateColumns: this.getTemplateColumns(), gridTemplateRows: this.getTemplateRows() }} className="grid-container">
                        {this.renderColumns()}
                        <Loading />
                    </div>
                </div>
            </Fragment>
        );
        const contextValue = {
            ...this.props,
            ...this.state,
            loadByQuery: this.loadByQuery,
            changeStore: this.changeStore,
            prevPage: this.prevPage,
            nextPage: this.nextPage,
            lastPage: this.lastPage,
            firstPage: this.firstPage,
            changePageSize: this.changePageSize,
        }
        return (
            <Fragment>
                <GridContext.Provider value={contextValue}>
                    <div className="a-grid" >
                        <div ref={this.dom} style={{ direction: 'rtl', gridTemplateColumns: this.getTemplateColumns(), gridTemplateRows: this.getTemplateRows() }} className="grid-container">
                            {/* {this.renderFilters()} */}
                            {this.renderColumns()}
                            {this.renderRows()}
                        </div>
                    </div>
                    <div className="a-grid-paginate">
                        <Paginate />
                    </div>
                </GridContext.Provider>
            </Fragment>
        );
    }
}
GridAction.defaultProps = {
    change: (val) => { }
}
mapStore(GridAction)
export default GridAction;