import React, {Component, createRef, Fragment} from 'react';
import './Grid.css';
import Loading from '../Loading/loading'
import Paginate from './Paginate';
import GridContext from './GridContext';
import $ from 'jquery';
import {mapStore} from '../../globalStore/GlobalStore'
import {Input} from '../Form';
import {EmptyData} from '../EmptyData/EmptyData';

class Grid extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading: false,
            pageSize: this.props.pageSize || 10,
            pageNumber: this.props.pageNumber || 1,
            dataLength: null,
            columns: this.createColumns(),
            getIndex: null,
            selectedRow: null,
            warrentPhone: null,
            warrentId: null
        }
        this.dom = createRef();
    }

    async componentDidMount() {
        const res = [...this.state.columns]
        let result = []
        res.forEach((node, i) => {
            if (node.filtering === true) {
                result.push(`#submitFilter-${i}`)
            }
        })
        result = result.join(",");
        $(result).keypress((e) => {

            if (e.key === "Enter") {
                e.preventDefault()
                let index = e.target.id.charAt(e.target.id.length - 1)
                this.sendFilter(Number(index))
            }
        })
        // const { phone } = this.props
        // if (!phone || phone === null) return null
        // await this.loadByQuery(() => {
        this.timer = setTimeout(() => {
            if (this.state.data && this.state.data !== null && this.state.data.length > 6) {
                $(this.dom.current).height($(window).height() - 300)
            }
        }, 100)
    }

    // componentDidUpdate(preProps, prevState) {
    //     if (preProps.phone !== this.props.phone) {
    //         this.setState({
    //             phoneNumber: this.props.phone,
    //             warrentId: this.props.warrentIds
    //         })
    //     }
    // }
    componentWillUnmount() {
        clearTimeout(this.timer)
    }

    removeRow = async (row, index) => {
        const {remove} = this.props;
        const res = await remove(row, index + 1);
        if (res) {
            let rows = Array.from(this.state.data);
            rows.splice(index, 1);
            this.setState({
                data: rows
            })
            this.loadByQuery()
        }

    }


    createColumns = () => {
        const {columns} = this.props;
        return columns.map((o, i) => {
            return {
                showTitle: o.showTitle,
                title: o.title,
                filterType: 3,
                filterText: '',
                dataType: o.type,
                // tableName: o.tableName,
                // fieldName: o.fieldName,
                sorting: o.sorting,
                filtering: o.filtering,
                sort: {show: false, desc: false},
                search: false,
                select: false,

            }
        });
    }
    reload = () => {
        const {reloaded} = this.props;
        this.loadByQuery(() => {
            reloaded()
        });
    }
    loadByQuery = async (cb) => {
        const {actions} = this.context;
        const {api, id, removePhone,} = this.props;
        const {pageSize, pageNumber, columns} = this.state;
        const res = await api({pageSize, pageNumber, columns});
        actions.loadGrid(id, {...this.props, items: res})
        await cb && cb()
        if ((!res && res === null)) {
            this.setState({data: null})
        } else if (res.length === 0)
            this.setState({data: []})
        else
            this.setState({
                data: res.Items,
                dataLength: res.pageCount,
            })
        this.setState({loading : false});
    }
    SetSearch = () => {
        const {phone, warrentId} = this.props
        this.setState(prevState => {
            return {
                warrentPhone: phone,
                warrentId: warrentId,
                pageNumber: 1,
                data: null,
                loading: true
            }
        }, () => {
            this.loadByQuery()
        })
        // this.loadByQuery()
    }
    removeAllFilters = () => {

        let columns = Array.from(this.state.columns);
        columns.forEach(o => {
            o.filterText = '';
        })

        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });
    }


    createActions = (row, index) => {
        const {edit, like} = this.props;
        return (
            <div className="grid-cell thin">
                {/* <div style={{ display: 'flex' }}> */}
                <div>
                    <button onClick={() => {
                        edit(row)
                    }}>
                        <span><i className="fa fa-eye"></i></span>
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    {/* <Like post={{postLike:0}} handleLike={like.bind(this, row)} /> */}
                    {/* <button onClick={this.removeRow.bind(this, row, index)}>
                        <span><i className="fa fa-thumbs-o-up"></i></span>
                    </button> */}
                </div>
            </div>
        )
    }


    handleSort = (i) => {
        const columns = Array.from(this.state.columns);
        if (columns[i].sort.show === false) {
            columns.forEach(o => {
                o.sort.show = false;
                o.sort.desc = false;
            });
            columns[i].sort.show = true;
        } else if (columns[i].sort.show === true && columns[i].sort.desc === true) {
            columns.forEach(o => {
                o.sort.show = false;
                o.sort.desc = false;
            });

        } else if (columns[i].sort.show === true) {
            columns[i].sort.desc = true

        }
        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });

    }

    changeFilterType(i, e) {
        const columns = Array.from(this.state.columns);
        columns[i].filterType = Number(e.target.value);
        this.setState({columns})
    }

    changeFilterText(i, e, val) {
        const columns = Array.from(this.state.columns);
        columns[i].filterText = e.target.value
        this.setState({columns})
    }

    sendFilter = (i, e) => {
        const columns = Array.from(this.state.columns);
        columns[i].search = false;
        // columns[i].filterText = "";
        columns.forEach((node, index) => {
            if (index !== i) {
                node.filterText = ""
            }
        })
        this.setState(prevState => {
            return {
                columns,
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
        });
    }

    clearFilter(i, e) {
        const columns = Array.from(this.state.columns);
        //columns[i].filterType =1;
        columns[i].filterText = '';

        this.setState(prevState => {
            return {
                columns
            }
        }, () => {
            this.loadByQuery()
        });
    }

    renderFilters = () => {
        const {columns} = this.state;
        const cols = columns.map((o, i) => {
            return (
                <div className="grid-cell" key={i}>
                    <div className="filter-area">
                        <div>
                            <Input
                                label={o.showTitle}
                                bg={true}
                                value={o.filterText}
                                left={true}
                                type="text"
                                required={false}
                                change={this.changeFilterText.bind(this, i)}
                            />
                        </div>
                        {/* <input placeholder="جستجو ..." value={o.filterText} onChange={this.changeFilterText.bind(this, i)} /> */}
                        <div style={{display: 'flex'}}>
                            <button type="button" onClick={this.sendFilter.bind(this, i)}>
                                <span className="icon">
                                    <i className="fa fa-filter"></i>
                                </span>
                            </button>
                            <button type="button" onClick={this.clearFilter.bind(this, i)}>
                                <span className="icon">
                                    <i className="fa fa-eraser"></i>
                                </span>
                            </button>
                            <select defaultValue={o.filterType} className="browser-default"
                                    onChange={this.changeFilterType.bind(this, i)}>
                                <option value="1">برابر است با</option>
                                <option value="2">برابر نیست با</option>
                                <option value="3">شامل</option>
                                <option value="4">شامل نمیشود</option>
                            </select>
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="grid-row">
                <div className="grid-cell thin">
                    <button style={{margin: "7px 1px"}} type="button" onClick={this.removeAllFilters}>
                        <span className="icon">
                            <i className="fa fa-eraser"></i>
                        </span>
                    </button>
                </div>
                <div className="grid-cell thin"></div>
                {cols}
            </div>
        )
    }
    firstPage = () => {
        const {pageNumber, dataLength, pageSize} = this.state;

        this.setState(prevState => {
            return {
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
        });
    }
    lastPage = () => {
        const {pageNumber, dataLength, pageSize} = this.state;

        this.setState(prevState => {
            return {
                pageNumber: Math.ceil(dataLength / Number(pageSize))
            }
        }, () => {
            this.loadByQuery()
        });
    }
    nextPage = () => {
        const {pageNumber, dataLength, pageSize} = this.state;
        if (pageNumber >= Math.ceil(dataLength / Number(pageSize))) return;

        this.setState(prevState => {
            return {
                pageNumber: prevState.pageNumber + 1
            }
        }, () => {
            this.loadByQuery()
        });
    }
    prevPage = () => {
        const {pageNumber} = this.state;
        if (pageNumber <= 1) return;

        this.setState(prevState => {
            return {
                pageNumber: prevState.pageNumber - 1
            }
        }, () => {
            this.loadByQuery()
        });

    }
    changePageSize = (size) => {
        this.setState(prevState => {
            return {
                pageSize: Number(size),
                pageNumber: 1
            }
        }, () => {
            this.loadByQuery()
        });
    }

    renderRows = () => {
        const {data} = this.state;
        return data.map((o, i) => {
            return this.renderRow(o, i)
        })
    }
    renderRow = (row, index) => {
        const {columns} = this.state;
        const {rowIndex} = this.props;
        const rows = columns.map((o, i) => {
            return (
                <div key={i} className="grid-cell" title={row[o.title]}>
                    <div className="grid-cell-text">
                        {row[o.title]}
                    </div>
                </div>)
        });
        return (
            <div
                className={rowIndex === index ? `grid-row table-row-select` : `grid-row ${index % 2 === 0 ? "table-row-odd" : "table-row-even"}`}
                key={index}
                onClick={() => {
                    this.props.getIndex(index, row)
                    // this.setState(this.handleRowIndex.bind(this, index))
                }}>
                {this.createActions(row, index)}
                <div className="grid-cell thin" style={{padding: "8px 15px"}}>{index + 1}</div>
                {rows}
            </div>
        )
    }
    renderColumns = () => {
        const {columns} = this.state;
        const cols = columns.map((o, i) => {
            return (
                <div className="grid-cell search-input" style={{display: "flex"}} key={i}>
                    <div>
                        {o.showTitle}
                    </div>
                    {o.sorting && <div className="grid-sort" onClick={this.handleSort.bind(this, i)} key={i}>
                        {o.sort.show && !o.sort.desc ?
                            (<span style={{fontSize: 20}}> <i className="fa fa-sort-asc"></i> </span>) :
                            o.sort.show && o.sort.desc ?
                                (<span style={{fontSize: 20}}> <i className="fa fa-sort-desc"></i> </span>) : (
                                    <span style={{fontSize: 20}}><i className="fa fa-sort" aria-hidden="true"></i></span>)}
                        {/* {o.sort.show && !o.sort.desc && <span style={{ fontSize: 20 }}>&#8679;</span>}
                        {o.sort.show && o.sort.desc && <span style={{ fontSize: 20 }}>&#8681;</span>} */}
                    </div>}
                    {o.filtering &&
                    <>
                    <span style={{fontSize: 14, cursor: "pointer", margin: "0 10px"}}
                          onClick={this.handleSearchShow.bind(this, i)}><i className="fa fa-search"
                                                                           aria-hidden="true"></i></span>
                        {o.filterText !== "" && <span style={{fontSize: 14, cursor: "pointer", margin: "0 10px"}}
                                                      onClick={this.handleSearchShow.bind(this, i)}><i
                            className="fa fa-filter" aria-hidden="true"></i></span>}
                        <div className={o.search ? "search-triangle" : "search-triangle display"}></div>
                        <div className={o.search ? "search-input__style" : "search-display"}>
                            <input id={`submitFilter-${i}`} placeholder="جستجو ..." value={o.filterText}
                                   onChange={this.changeFilterText.bind(this, i)}/>
                            <button id="submitBtn_filter" className="search_btn"
                                    onClick={this.sendFilter.bind(this, i)}>
                                <span><i className="fa fa-search"></i></span>
                            </button>
                        </div>
                    </>
                    }
                </div>
            )
        })
        return (
            <Fragment>
                <div className="grid-row table-header">
                    <div className="grid-cell thin" style={{padding: "8px 15px"}}>عملیات</div>
                    <div className="grid-cell thin" style={{padding: "8px 15px"}}>ردیف</div>
                    {cols}
                </div>
            </Fragment>
        )
    }
    rednerEmptyData = () => {
        return (
            <div className="empty_data">
                <div>
                    <span className="icon"><i className="fa fa-hdd-o"></i></span>
                </div>
                <span>دیتا برای نمایش وجود ندارد</span>
            </div>
        )
    }
    handleSearchShow = (i, e) => {
        const columns = Array.from(this.state.columns)
        columns[i].search = !columns[i].search;
        // columns[i].filterText = ""
        this.setState({columns})
    }
    getTemplateColumns = () => {
        let str = '90px 90px ';
        const {columns} = this.state;
        columns.forEach(o => {
            str += '250px '
        });
        return str;
    }
    getTemplateRows = () => {
        let str = '85px 85px';
        const {columns} = this.state;
        columns.forEach(o => {
            str += '40px '
        });
        return str;
    }

    render() {
        const {data, warrentPhone, warrentId} = this.state;
        const {reload} = this.props;
        if (reload) {
            this.reload()
        }
        const contextValue = {
            ...this.props,
            ...this.state,
            loadByQuery: this.loadByQuery,
            changeStore: this.changeStore,
            prevPage: this.prevPage,
            nextPage: this.nextPage,
            lastPage: this.lastPage,
            firstPage: this.firstPage,
            changePageSize: this.changePageSize,
        }
        return (
            <Fragment>
                <div className="warrent-phone-search"
                     style={{display: "inline-block", marginRight: "6px", marginTop: "1px"}}>
                    <button type="submit" onClick={this.SetSearch} id="sbWarrentPhoneNumber">
                        <span className="icon">
                            <i className="fa fa-search"></i>
                        </span>
                    </button>
                </div>
                <div style={{display: "block", height: "5px"}}></div>
                <GridContext.Provider value={contextValue}>
                    <div className="a-grid">
                        <div ref={this.dom} style={{
                            direction: 'rtl',
                            gridTemplateColumns: this.getTemplateColumns(),
                            gridTemplateRows: this.getTemplateRows()
                        }} className="grid-container">
                            {/* {this.renderFilters()} */}
                            {this.renderColumns()}
                            {this.state.loading ? <Loading/> : data === null ? null : data.length === 0 ?
                                <EmptyData text={"حکم با مشخصات ذکر شده یافت نشد"}/> : this.renderRows()}
                        </div>
                    </div>
                    <div className="a-grid-paginate">
                        <Paginate/>
                    </div>
                </GridContext.Provider>
            </Fragment>
        );
    }
}

Grid.defaultProps = {
    change: (val) => {
    }
}
mapStore(Grid)
export default Grid;
