import React from 'react'

export const EmptyData = ({ text }) => {
    const divStyle = {
        textAlign: "center",
        color: "#989898",
        marginTop: "20px",
    }
    const iconStyle = {
        fontSize: 60
    }
    return (
        <div style={divStyle}>
            <div>
                <span className="icon" style={iconStyle}>
                    <i className="fa fa-database"></i>
                </span>
            </div>
            <div>
                <span>{text}</span>
            </div>
        </div>
    );
}
export const EmptyTask = ({ text }) => {
    const divStyle = {
        textAlign: "center",
        color: "#000",
        direction: "rtl"
    }
    const iconStyle = {
        fontSize: 25,
        marginLeft: "5px"
    }
    return (
        <div style={divStyle}>
            <span className="icon" style={iconStyle}>
                <i className="fa fa-database"></i>
            </span>
            <span>{text}</span>
        </div>
    );
}

