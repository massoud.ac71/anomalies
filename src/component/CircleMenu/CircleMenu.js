import React, {PureComponent, Fragment} from 'react'
import './CircleMenu.css'
import Tools from "../../data/Access";
const menu = [
    {id: 0, title: "سامانه سما", status: false ,name:"Sama" },
    {id: 1, title: "سامانه توزیع", status: false , name:"Dist"},
    {id: 2, title: "SIP", status: false , name : "SIP"},
    {id: 3, title: null, status: false},
    {id: 4, title: null, status: false},
    {id: 5, title: null, status: false},
    {id: 6, title: null, status: false},
    {id: 7, title: null, status: false},
    {id: 8, title: null, status: false},
    {id: 9, title: null, status: false},
    {id: 10, title: null, status: false},
    {id: 11, title: null, status: false},
    {id: 12, title: null, status: false},
    {id: 13, title: null, status: false},
    {id: 14, title: null, status: false},
    {id: 15, title: null, status: false},
    {id: 16, title: null, status: false},
    {id: 17, title: null, status: false},
    {id: 18, title: null, status: false},
    {id: 19, title: null, status: false},
    {id: 20, title: null, status: false},
    {id: 21, title: null, status: false},
]

class CircleMenu extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount = () => {
        const {handleShowMenu} = this.props
        this.timer = setTimeout(() => {
            handleShowMenu()
        }, 500);
    }
    componentWillUnmount = () => {
        clearTimeout(this.timer)
    }

    render() {
        const {handleSystemService, circleMenu, handleShowMenu} = this.props
        const {Service} =Tools();
        menu.forEach(n=>{
            for(let [key,value] of Object.entries(Service)){
                if(key===n.name && value === true) n.status=true;
            }
        })
        return (
            <Fragment>
                <div className={!circleMenu ? "circle-menu " : "circle-menu active"}>
                    <div className="cricle-btn trigger" onClick={handleShowMenu}>
                        <span className="circle-title">
                            <i className="fa fa-cogs" style={{fontSize: "3em"}}/>
                        </span>
                    </div>
                    <div className="circle-icons">
                        {menu.map((node, i) => {
                            return (
                                <div className="rotater" key={node.id}
                                     onClick={node.status ? handleSystemService.bind(this, node) : null}>
                                    <div
                                        className={node.status === true ? "cricle-btn btn-icon" : "cricle-btn btn-icon disabled"}>
                                        <span className="circle-title">{node.status && node.title}</span>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default CircleMenu;
