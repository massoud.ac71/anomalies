import React, { Component, createRef } from "react";
import "./ModalText.css";
// import $ from 'jquery'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dir: "rtl"
        }
        this.modelDom = createRef();
    }
    componentDidMount() {
        const { description } = this.props;
        if (description?.length > 0) {
            var firstChar = description?.charCodeAt(0);
            if (firstChar >= 1536 && firstChar <= 1791) {
                this.setState({ dir: 'rtl' })
            }
            else {
                this.setState({ dir: 'ltr' })
            }
        }
    }
    render() {
        const { description } = this.props

        // if (!$("body").hasClass("r-modal-open")) {
        //     $("body").addClass("r-modal-open");
        // }
        // const breakLine = "\n";
        let data = null
        if (description === null) data = ""
        const text = data !== null ? data : description.replace("null", "")
        return (
            <div className="r-modal__description">
                <div className="r-modal__description-wrapper">
                    <div className="r-modal__description-header">
                        {/* <h2>1</h2> */}
                        <div className="r-modal__description-close" >
                            <div className="r-modal__description-inner-close" onClick={this.props.closeModal} >
                                <span className="icon">
                                    <i className="fa fa-close" aria-hidden="true">
                                    </i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="r-modal__description-content" ref={this.modelDom}
                        //  style={{direction: `${this.state.dir}`}}
                        style={{ direction: "auto" }}
                    >
                        {text}
                    </div>
                </div>
            </div>
        );
    }
}
Modal.defaultProps = {
    change: (val) => { },
};

export default Modal;
