import React, { Component, createRef } from "react";
import $ from "jquery";

// Component
import { Select } from "../Form";
import Loading from "../Loading/loading";
import { mapStore } from "../../globalStore/GlobalStore";
import { Posts } from "../../services/RestApi";
import DynamicLayout from "../ReactLayout/Layout";
// Css
import "./ModalDashboard.css"

// Utils
import Toast from '../../utils/Toast'
import Handler from '../../utils/Handler';
import ToastContainers from '../../utils/ToastContainer';
import Apis from '../../data/Api';

class ModalDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: null,
            title: "در حال بارگذاری",
            line: {
                V_1: 5,
                V_2: 25,
                V_3: 29,
                V_4: 17,
                V_5: 5,
                V_6: 0,
                V_7: 5,
                V_8: 11,
                V_9: 0
            },
            modal: false,
            assignWidget: null,
            widgetState: null
        };
        this.modelDom = createRef();
    }
    handleAssign = (value, event) => {
        this.setState({ modal: true, widgetState: value })
    }
    changeHandler = (field, value, item) => {
        this.setState({ assignWidget: item })
    }
    componentDidMount() {
        const { save, cancel } = this.props;
        this.load();
        $('body').keyup((e) => {
            if ($('body').hasClass('r-modal-open')) {
                if (e.keyCode === 13) {
                    setTimeout(() => {
                        this.save.call(this)
                    }, 1000)

                }
                else if (e.keyCode === 27) {
                    cancel()
                }
            }
        })
    }
    delay = (t, cb) => {
        setTimeout(cb, t);
    };
    load = async (cb) => {
        const { actions } = this.context;
        const { api, id } = this.props;
        const res =await api();
        cb && cb();
        await actions.loadModal(id, { ...this.props, ...res });
        this.setState({
            items: res.data,
            title: "لیست ویجت های طراحی شده"
        })
    };
    save = async () => {
        const { assignWidget, widgetState } = this.state
        const { active, reloadDashboard, closeModal } = this.props
        if(assignWidget ===null && active === null) {
            Toast.ToastError("لطفا یک داشبورد انتخاب کنید" , "F")
            return 
        }
        let request = {
            WidgetId: widgetState.widgetId,
            DashboardId: (assignWidget && assignWidget.id) ? assignWidget.id : (active && active.dashboardId) ? active.dashboardId : 0
        }
        try {
            Handler.apiHandler(await Posts(Apis.AddWidgetToDashboard, request), (response, status) => {
                if (response && response.value.statusCode === 300) {
                    Toast.ToastWarining("ویجت مورد نظر در داشبورد موجود است", "F")
                }
                else
                    if (response.statusCode === 200) {
                        Toast.ToastSuccess("ویجت با موفقیت به داشبورد اضافه شد", "E")
                        let reload = {
                            dashboardId: request.DashboardId
                        }
                        reloadDashboard(reload)
                        closeModal()
                        this.setState({
                            modal: false,
                            assignWidget: null,
                            widgetState: null
                        })
                    }
            })
        } catch (error) {
        }
        // save({
        //     items,
        //     title,
        //     id: rowId,
        // });
    };
    createLayout = () => {
        const { items } = this.state
        const { handleDelete } = this.props
        items.forEach((o, i) => {
            o.tiles.sort((a, b) => {
                return a.order - b.order
            })
        })
        return (
            <div className="modal_widget_render">
                <DynamicLayout items={items} handleDelete={handleDelete} handleAssign={this.handleAssign}/>
            </div>
        )
    }
    renderList = () => {
        const { list, active } = this.props
        const data = {
            i: 1,
            field: 'dashboard', type: 'select', label: 'داشبورد', value: "", required: true,
            items: list.map((o, i) => {
                return { id: o.dashboardId, name: o.title }
            }),
            mapping: { text: 'name', value: 'id' }
        };
        return (
            <div className="model-item">
                <Select
                    type={data.type}
                    key={data.i}
                    required={data.required}
                    items={data.items}
                    mapping={data.mapping}
                    label={data.label}
                    value={active && active.dashboardId ? active.dashboardId : ""}
                    change={this.changeHandler.bind(this, data.field, data)}
                />
            </div>
        );
    }
    reload = () => {
        const { reloaded } = this.props
        this.load(() => {
            reloaded()
        })
    }
    render() {
        const { close, reload } = this.props;
        const { items, title, modal } = this.state;
        if (!$("body").hasClass("r-modal-dash-open")) {
            $("body").addClass("r-modal-dash-open");
        }
        if (reload) { this.reload() }
        return (
            <div className="r-modal-dash">
                <div className="r-modal-wrapper">
                    <ToastContainers position={"top-right"} containerId={"E"} />
                    <div className="r-modal-header">
                        <h2>{title}</h2>
                        <span className="fa fa-close" onClick={close}></span>
                    </div>
                    <div className="r-modal-dash-content" ref={this.modelDom}>
                        {" "}
                        {items === null ? <Loading /> : this.createLayout()}{" "}
                        {modal && <div className="r-modal-assign">
                            <div className="r-modal-assign-wrapper">
                                <ToastContainers position={"top-right"} containerId={"F"} />
                                <div className="r-modal-assign-header">
                                    <h2>اتصال ویجت طراحی شده با داشبورد</h2>
                                </div>
                                <div className="r-modal-assign-content">
                                    {this.renderList()}
                                </div>
                                <div className="r-modal-assign-footer">
                                    <button onClick={this.save}>
                                        ذخیره
                                    </button>
                                    <button onClick={() => {
                                        this.setState({ modal: false })
                                    }}>
                                        انصراف
                            </button>
                                </div>
                            </div>
                        </div>}
                    </div>
                    {items !== null && (
                        <div className="r-modal-footer" style={{ backgroundColor: "var(--Basic)", height: "10px" }} >
                        </div>
                    )}
                </div>

            </div>
        );
    }
}
ModalDashboard.defaultProps = {
    change: (val) => { },
};
mapStore(ModalDashboard);
export default ModalDashboard;
