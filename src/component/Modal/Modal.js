import React, { Component, createRef } from "react";
import {  Input, Select, DatePicker, CheckBox } from "../Form";
import { mapStore } from "../../globalStore/GlobalStore";
import Loading from "../Loading/loading";
import $ from "jquery";
import "../Modal/Modal.css"

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: null,
      title: "در حال بارگذاری",
    };
    this.modelDom = createRef();
  }

  changeHandler = (field, val) => {
    
    const { change } = this.props;
    this.state.items.filter((o) => {
      return o.field === field;
    })[0].value = val;

    this.setState(
      () => {
        return {
          items: this.state.items,
        };
      },
      () => {
        change(this.state.items);
      }
    );
  };

  componentDidMount() {
    this.load();
  }

  delay = (t, cb) => {
    setTimeout(cb, t);
  };

  load = async (cb) => {
    const { actions } = this.context;
    const { api, id, rowId } = this.props;
    const res = await api(rowId);
    cb && cb();
    await actions.loadModal(id, { ...this.props, ...res });
    this.setState({
      items: res.items,
      title: res.title,
    });
  };
  createForm = () => {
    const { items } = this.state;
    const formItems = items.map((o, i) => {
      let req =
        typeof o.required === "boolean" ? o.required : JSON.parse(o.required);
      if (o.type === "text") {
        return (
          <div className="model-item">
            <Input
              type={o.type}
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      } else if (o.type === "text1") {
        return (
          <div className="model-item">
            <Input
              type={o.type}
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      } else if (o.type === "select") {
        return (
          <div className="model-item">
            <Select
              type={o.type}
              key={i}
              required={req}
              items={o.items}
              mapping={o.mapping}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      }
      else if (o.type === "selectcolor") {
        return (
          <div className="model-item">
            <Select
              key={i}
              required={req}
              items={o.items}
              mapping={o.mapping}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      } else if (o.type === "datepicker") {
        return (
          <div className="model-item">
            <DatePicker
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      } else if (o.type === "checkbox") {
        
        return (
          <div className="model-item">
            <CheckBox
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      }
      else if (o.type === "number") {
        return (
          <div className="model-item">
            <Input
              type={o.type}
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      }
      else if (o.type === "textarea") {
        return (
          <div className="model-item">
            <Input
              type={o.type}
              key={i}
              required={req}
              label={o.label}
              value={o.value}
              change={this.changeHandler.bind(this, o.field)}
            />
          </div>
        );
      }
    });
    return (
      <form autoComplete="off">
        <div className="modal-item-wrapper">{formItems}</div>
      </form>
    );
  };
  save = () => {
    const { save, rowId } = this.props;
    const { items, title } = this.state;

    save({
      items,
      title,
      id: rowId,
    });
  };
  render() {
    const { save, cancel, close } = this.props;
    const { items, title } = this.state;
    if (!$("body").hasClass("r-modal-open")) {
      $("body").addClass("r-modal-open");
    }
    return (
      <div className="r-modal">
        <div className="r-modal-wrapper">
          <div className="r-modal-header">
            <h2>{title}</h2>
            <span className="fa fa-close" onClick={close}></span>
          </div>
          <div className="r-modal-content" ref={this.modelDom}>
            {" "}
            {items === null ? <Loading /> : this.createForm()}{" "}
          </div>
          {items !== null && (
            <div className="r-modal-footer">
              {/* <Button action={this.save} text="ذخیره" /> */}
              <button onClick={this.save}>
                ذخیره
              </button>
              <button onClick={cancel}>
                انصراف
              </button>
              {/* <Button className="default" action={cancel} text="انصراف" /> */}
            </div>
          )}
        </div>
      </div>
    );
  }
}
Modal.defaultProps = {
  change: (val) => { },
};
mapStore(Modal);
export default Modal;
