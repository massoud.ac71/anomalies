import styled from 'styled-components';

// Modal
export const Modal = styled.div`
    background: rgba(0, 0, 0, 0.6);
    position: fixed;
    top: 0;
    left: 0;
    marginTop: ${props => props.marginTop}%;
    width: 100%;
    height: 100%;
    z-index: ${props => props?.zIndexs ? props.zIndexs : 99};`

// Props => heigth={500} width={700} bgColor={""}
export const ModalWrapper = styled.div`
    background: #fff;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
    width: ${props => props.width}px;
    height:  ${props => props.heigth}px;
    margin: ${props => props?.marginTop ? props?.marginTop : 5}% auto 0 auto;
    overflow: hidden;
    animation: minimise 1000ms linear;
    -webkit-animation: minimise 1000ms linear;
    -moz-animation: minimise 1000ms linear;
    animation-name: bounce;
    -webkit-animation-name: bounce;
    -moz-animation-name: bounce;
    transition: all 0.4s linear;
    z-index: ${props => props?.zIndexs ? props.zIndexs : 999};`


// Props => Header heigth={50} bgColor={""} textColor={""}
export const ModalHeader = styled.div`
    display: flex;
    background: ${props => props.bgColor};
    align-items: center;
    justify-content: space-between;
    padding: 0 24px;
    color:${props => props.textColor};
    height:  ${props => props.heigth}px;  
    `

//Props => Content heigth={400} bgColor={"#FFFFFF"}
export const ModalContent = styled.div`
    height: ${props => props.heigth}px;  
    width: 100%;
    overflow: hidden;
    transition: all 0.4s linear;
    `

//Props => Footer heigth={50} bgColor={""} textColor={""} btnBg={""} btnHover={""}
export const ModalFooter = styled.div`
    height:  ${props => props.heigth}px;  
    width: 100%;
    background: ${props => props.bgColor};
    transition: all 0.4s linear;
    &  button ,
    & > a {
    padding: 8px 15px;
    display: inline-block;
    background:${props => props.btnBg};
    font-size:12px;
    font-family:inherit;
    color:${props => props.textColor};
    textAlign: center;
    outline: none;
    border : 1px solid ${props => props.btnBg} ;
    border-radius : 3px;
    margin: 3px 5px;
    cursor: pointer;
}
&  button:hover,
& > a:hover {
    background: ${props => props.btnHover};
    border : 1px solid ${props => props.btnHover};
    color :${props => props.textColorHover};
    transition: all 0.5s
}

`