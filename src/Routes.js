import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom"
//Component//

import Notfound from './component/NotFound/NotFound'
import MainMenu from './pages/Mainmenu/mainmenu';
import RegisterData from './pages/RegisterData/registerData';
import TrainedModels from './pages/TrainedModels/trainedModels';

const Routes = ({ isAuth }) => {
    return (
        // <Router>
        <Fragment>
            <Switch>
                <Route exact path="/" component={MainMenu} />
                <Route exact path="/metricAnalyzer/registered-data" component={RegisterData}/>
                <Route exact path="/metricAnalyzer/trained-models" component={TrainedModels} />
                <Route component={Notfound} />
            </Switch>
            {/* {!isAuth && <Redirect exact from="*" to="/" />} */}
        </Fragment>
        // </Router>
    );
}

export default Routes;
