window.env = {
    SERVER_IPADDRESS: `http://localhost`,
    PORT: `5001`,
    PROXY_IPADDRESS: `http://localhost`,
    PROXY_PORT: `5004`
}
